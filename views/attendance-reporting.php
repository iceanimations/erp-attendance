<?php
	global $wpdb;

    $selected_query_time       = isset( $_REQUEST['query_time'] ) ? $_REQUEST['query_time'] : 'this_month';
    $selected_location         = isset( $_REQUEST['location'] ) ? $_REQUEST['location'] : '';
    $selected_department       = isset( $_REQUEST['department'] ) ? $_REQUEST['department'] : '';
    $selected_start            = isset( $_REQUEST['start'] ) ? $_REQUEST['start'] : '';
    $selected_end              = isset( $_REQUEST['end'] ) ? $_REQUEST['end'] : '';
    $duration                  = [];

    if ( 'custom' == $selected_query_time ) {
        $duration['start'] = $selected_start;
        $duration['end']   = $selected_end;
    } else {
        $duration = erp_att_get_start_end_date( $selected_query_time );
    }

    $query_user                = "SELECT user_id FROM {$wpdb->prefix}erp_hr_employees";
    $user_ids                  = $wpdb->get_col( $query_user );
    $locations                 = erp_company_get_location_dropdown_raw();
    $departments               = erp_hr_get_departments();
    $dept_dropdown_raw         = erp_hr_get_departments_dropdown_raw();
    $office_time               = erp_att_get_office_time();
    $query_times               = erp_att_get_query_times();
//    $shifts                    = erp_att_get_shifts();
    $grace_times               = erp_att_get_grace_times();
    $attendance_record         = erp_att_get_attendance_by_daterange( $duration['start'], $duration['end'] );
    $periods                   = calculate_date_period( $duration['start'], $duration['end'] );
    $total_days                = iterator_count( $periods );
//    $is_shift_enabled          = erp_get_option( 'enable_shift', 'erp_settings_erp-hr_attendance' );
    $att_by_dept_all           = [];
    $att_data                  = [];
    $att_by_dept               = 0;
    $count_working_days        = erp_hr_get_work_days_between_dates( $duration['start'], $duration['end'] )['total'];
    $count_holidays            = 0;
    $shift_start               = 0;
    $shift_end                 = 0;
    $count_present             = 0;
    $count_regular             = 0;
    $count_holidays            = 0;
    $count_leave               = 0;
    $count_absent              = 0;
    $count_late                = 0;
    $count_early_left          = 0;
    $average_present_count     = 0;
    $average_regular_count     = 0;
    $average_absent_count      = 0;
    $average_leave_count       = 0;
    $average_late_count        = 0;
    $average_early_left_count  = 0;


    if ( ( $selected_department && -1 != $selected_department ) || ( $selected_location && -1 != $selected_location ) ) {

        $query_user .= " WHERE";

        if ( $selected_department && -1 != $selected_department ) {
            $query_user .= " department = $selected_department";
        }

        if ( ( $selected_department && -1 != $selected_department ) && ( $selected_location && -1 != $selected_location ) ) {
            $query_user .= " AND";
        }

        if( $selected_location && -1 != $selected_location ) {
            $query_user .= " location = $selected_location";
        }

        $user_ids = $wpdb->get_col( $query_user );

        foreach ( $user_ids as $user_id ) {
            $att_by_dept = erp_att_get_attendance_record_between_dates( $duration['start'], $duration['end'], $user_id );

            foreach ( $att_by_dept as $single_att ) {
                $att_by_dept_all[] = $single_att;
            }
        }

        $attendance_record = $att_by_dept_all;
    }

    foreach ( $periods as $period ) {

        $date           = $period->format( 'Y-m-d' );
        $valid_work_day = erp_hr_get_work_days_between_dates( $date, $date );

        if ( !$valid_work_day['days'][0]['count'] ) {

            $att_data[$date] = 'holiday';
           $count_holidays++;
            continue;
        }

        foreach ( $user_ids as $user_id ) {

            $department_id = erp_att_get_user_dept( $user_id );
            $leave_exist   = erp_hrm_is_leave_recored_exist_between_date( $date, $date, $user_id );

            if ( $leave_exist ) {
                if ( !isset( $att_data[$date]['leave'] ) ) {
                    $att_data[$date]['leave'] = 1;
                } else {
                    $att_data[$date]['leave']++;
                }

                $count_leave++;

                continue;
            }

            foreach ( $attendance_record['shifts'] as $single ) {

                if ( $date == $single['date'] && $user_id == $single['user_id'] && 'no' == $single['present'] ) {

                    if ( !isset( $att_data[$date]['absent'] ) ) {
                        $att_data[$date]['absent'] = 1;
                    } else {
                        $att_data[$date]['absent']++;
                    }

                    $count_absent++;

                    continue 2;
                }

                if ( $date == $single['date'] && $user_id == $single['user_id'] && 'yes' == $single['present'] ) {

                    if ( !isset( $att_data[$date]['present'] ) ) {
                        $att_data[$date]['present'] = 1;
                    } else {
                        $att_data[$date]['present']++;
                    }

                    $count_present++;

                    // if ( !$single['checkout'] ) {
                    //    $att_data[$date] = 'error';
                    //     continue 2;
                    // }

                    // $count_working_days++;
                    $timestamp_checkin  = strtotime( $single['checkin'] );
                    $timestamp_checkout = strtotime( $single['checkout'] );

                    $unix_date = strtotime( $date );

                    if ( ! is_shift_enabled() ) {

                        $shift_start = strtotime( $office_time['starts'] );
                        $shift_end   = strtotime( $office_time['ends'] );

                    } else {

                        $shift_start = strtotime( $single['shift_start_time'] );
                        $shift_end   = strtotime( $single['shift_end_time'] );
                    }

                    $result = calculate_work_details_by_date( $shift_start, $shift_end, $single['checkin'], $single['checkout'], $grace_times );

                    if ( $result['late'] ) {

                        if ( !isset( $att_data[$date]['late'] ) ) {
                            $att_data[$date]['late'] = 1;
                        } else {
                            $att_data[$date]['late']++;
                        }

                        $count_late++;
                    }

                    else if ( $result['early_left'] ) {
                        if ( !isset( $att_data[$date]['early_left'] ) ) {
                            $att_data[$date]['early_left'] = 1;
                        } else {
                            $att_data[$date]['early_left']++;
                        }

                        $count_early_left++;
                    } else {
                        $count_regular++;
                    }

                    continue 2;
                }

                // $att_data[$date] = 'absent';
            }
        }
    }

if ( $count_working_days ) {
    $average_present_count     = number_format( $count_present / $count_working_days, 2);
    $average_regular_count     = number_format( $count_regular / $count_working_days, 2);
    $average_absent_count      = number_format( $count_absent / $count_working_days, 2);
    $average_leave_count       = number_format( $count_leave / $count_working_days, 2);
    $average_late_count        = number_format( $count_late / $count_working_days, 2);
    $average_early_left_count  = number_format( $count_early_left / $count_working_days, 2);
}
echo '<pre>';
// echo $count_regular . ',' . $count_leave . ',' . $count_absent . ',' . $count_late . ',' . $count_early_left;
// var_dump($att_data);
echo '</pre>';
?>
<div class="wrap">
    <div class="att-hr-report-area" style="width:95%">
    	<h2><?php _e( 'Attendance Report', 'erp-attendance' ); ?></h2>

        <div class="att-reportquery-form">
            <form method="get">
                <input type="hidden" name="page" value="erp-hr-reporting">
                <input type="hidden" name="type" value="attendance-report">
                <select name="location">
                <?php
                    foreach ( $locations as $id => $name ) {
                        echo '<option value="' . $id . '"' . selected( $selected_location, $id ) . '>' . $name . '</option>' ;
                    }
                ?>
                </select>
                <select name="department">
                <?php
                    foreach ( $dept_dropdown_raw as $id => $name ) {
                        echo '<option value="' . $id . '"' . selected( $selected_department, $id ) . '>' . $name . '</option>' ;
                    }
                ?>
                </select>
                <select name="query_time" id="att-reporting-query">
                <?php
                    foreach ( $query_times as $key => $value ) {
                        echo '<option value="' . $key . '"' . selected( $selected_query_time, $key ) . '>' . $value . '</option>' ;

                    }
                ?>
                </select>

                <?php
                    if ( 'custom' == $selected_query_time ) {
                ?>
                    <span id="custom-input"><span>From </span><input name="start" class="attendance-date-field" type="text" value="<?php echo $selected_start; ?>">&nbsp;<span>To </span><input name="end" class="attendance-date-field" type="text" value="<?php echo $selected_end ?>"></span>&nbsp;
                <?php
                    }
                ?>
                <?php wp_nonce_field( 'epr-att-hr-reporting-filter' ); ?>
                <button type="submit" class="button-secondary" name="filter_att_hr_reporting"><?php _e( 'Filter', 'erp' ); ?></button>
            </form>
        </div>

        <div class="erp-single-container" style="margin-top: 15px;">
    		<div class="erp-area-left">
    			<div class="postbox" style="padding: 20px;">
    				<div id="emp-att-hr-report">
                        <div id="att-report-primary" style="width:45%;height:350px;"></div>
                        <div id="att-report-secondary" style="width:45%;height:350px;"></div>
                    </div>
    			</div>
    		</div>
    	</div>

        <div class="postbox leads-actions att-hr-summary">
            <div class="handlediv" title="<?php _e( 'Summary', 'erp-attendance' ); ?>"><br></div>
            <h3 class="hndle"><span><?php _e( 'Summary', 'erp-attendance' ); ?></span></h3>
            <div class="inside">
                <ul class="erp-list two-col separated">
                    <li><?php erp_print_key_value( __( 'Total Days', 'erp-attendance' ), $total_days ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Work Days', 'erp-attendance' ), $count_working_days ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Holidays', 'erp-attendance' ), $count_holidays ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Present', 'erp-attendance' ), $average_present_count . ' %' ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Absent', 'erp-attendance' ), $average_absent_count . ' %' ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Leave', 'erp-attendance' ), $average_leave_count . ' %' ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Late', 'erp-attendance' ), $average_late_count . ' %' ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Early Leave', 'erp-attendance' ), $average_early_left_count . ' %' ); ?></li>

                </ul>
            </div>
        </div><!-- .postbox -->

        <div id="employee-attendance-table">
            <table class="widefat striped">
                <thead>
                    <tr>
                        <th><?php _e( 'Date', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Present', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Absent', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Late', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Early Leave', 'erp-attendance' ); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ( $att_data as $date => $single ) {
                            echo '<tr>';
                            echo '<td>' . $date . '</td>';

                            if ( 'holiday' == $single ) {
                                echo '<td>' . __( 'Holiday', 'erp-attendance' ) . '</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                            } else {
                                echo isset( $single['present'] ) ? '<td>' . $single['present'] . '</td>' : '<td>0</td>';
                                echo isset( $single['absent'] ) ? '<td>' . $single['absent'] . '</td>' : '<td>0</td>';
                                echo isset( $single['late'] ) ? '<td>' . $single['late'] . '</td>' : '<td>0</td>';
                                echo isset( $single['early_left'] ) ? '<td>' . $single['early_left'] . '</td>' : '<td>0</td>';
                            }
                            echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<script>
	(function( $ ) {
	    var primaryData = [
            // {
            //     label: "<?php _e( 'Present', 'erp-attendance' ) ?>",
            //     data: 70,
            //     color: '#ae82bd'
            // },

            {
                label: "<?php _e( 'Work Days', 'erp-attendance' ) ?>",
                data: '<?php echo $count_working_days; ?>',
                color: '#ae82bd'
            },
            {
                label: "<?php _e( 'Holiday', 'erp-attendance' ) ?>",
                data: '<?php echo $count_holidays ?>',
                color: '#67c4cc'
            }
        ];

        var secondaryData = [
            {
                label: "<?php _e( 'Regular', 'erp-attendance' ) ?>",
                data: '<?php echo $average_regular_count; ?>',
                color: '#1e487b'
            },
            {
                label: "<?php _e( 'Leave', 'erp-attendance' ) ?>",
                data: '<?php echo $average_leave_count; ?>',
                color: '#fcc1b6'
            },
            {
                label: "<?php _e( 'Late', 'erp-attendance' ) ?>",
                data: '<?php echo $average_late_count; ?>',
                color: '#993232'
            },
            {
                label: "<?php _e( 'Early Leave', 'erp-attendance' ) ?>",
                data: '<?php echo $average_early_left_count; ?>',
            },
            {
                label: "<?php _e( 'Absent', 'erp-attendance' ) ?>",
                data: '<?php echo $average_absent_count; ?>',
                color: '#acdaac'
            },
        ];

        var options = {
            series: {
                pie: {
                    show: true,
                    label: {
                        show: true,
                        radius: 0.8,
                        formatter: function (label, series) {
                            return '<div style="border:1px solid grey;font-size:8pt;text-align:center;padding:5px;color:white;">' +
                            label + ' : ' +
                            Math.round(series.percent) +
                            '%</div>';
                        },
                        background: {
                            opacity: 0.8,
                            color: '#000'
                        }
                    }
                }
            }
        };

        var plot = $.plot($("#att-report-primary"), primaryData, options);
        var plot = $.plot($("#att-report-secondary"), secondaryData, options);

		var previousPoint = null, previousLabel = null;

	    $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];

                        var color = item.series.color;

                        showTooltip(item.pageX,
                        item.pageY,
                        color,
                        "<strong>" + item.series.yaxis.ticks[item.dataIndex].label + "</strong><br>" + item.series.label + " : <strong>" + x + "</strong>");
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };

        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '9px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }

	})(jQuery);
</script>

