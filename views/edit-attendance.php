<div class="wrap">
    <h2><?php _e( 'Edit Attendance', 'erp-attendance' ); ?></h2>

    <div id="erp-edit-attendance" v-cloak>
        <div style="display: flex; justify-content: space-between; width: 100%">
            <div>
                <?php printf('<b><span>%s</span><span>%s</span></b>', _e( 'Date: ', 'erp-attendance' ), $_REQUEST['edit_date'] ); ?>
            </div>
            <div>
                <label><input @click="makeAllPresent" v-model="allPresent" type="radio"><?php _e( 'Set all present', 'erp-attendance' ); ?></label>
                <label><input @click="makeAllAbsent" v-model="allAbsent" type="radio"><?php _e( 'Set all absent', 'erp-attendance' ); ?></label>
            </div>
            <div>
                <form id="search">
                    <?php _e( 'Search', 'erp-attendance' ); ?> <input name="query" v-model="searchQuery">
                </form>
            </div>
        </div>

        <table class="widefat striped">
            <thead>
            <tr>
                <th><?php _e( 'Employee ID', 'erp-attendance' ); ?></th>
                <th><?php _e( 'Employee Name', 'erp-attendance' ); ?></th>
                <?php if(is_shift_enabled()) { ?>
                <th><?php _e( 'Shift', 'erp-attendance' ); ?></th>
                <?php } ?>
                <th><?php _e( 'Status', 'erp-attendance' ); ?></th>
                <th><?php _e( 'Checkin', 'erp-attendance' ); ?></th>
                <th><?php _e( 'Checkout', 'erp-attendance' ); ?></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="att in attendance | filterBy searchQuery">
                <td>{{att.employee_id}}</td>
                <td>{{att.employee_name}}</td>
                <?php if(is_shift_enabled()) { ?>
                <td>{{att.shift}}</td>
                <?php } ?>
                <td>
                    <label><input v-model="att.present" value="yes" type="radio">Present</label>
                    <label><input v-model="att.present" value="no" type="radio">Absent</label>
                </td>
                <td>
                    <input style="width: 86px;" v-model="att.checkin" type="text" :disabled="'yes' == att.present ? false : true" v-erp-timepicker>
                </td>
                <td>
                    <input style="width: 86px;" v-model="att.checkout" type="text" :disabled="'yes' == att.present ? false : true" v-erp-timepicker>
                </td>
            </tr>
            </tbody>
        </table>

    </div>

    <div class="save-button-container">

    </div>
    <button id="save-attendance-edit" class="button-primary"><?php _e( 'Save Changes', 'erp-attendance' ); ?></button>  <span class="spinner" style="float: none;"></span>
</div>