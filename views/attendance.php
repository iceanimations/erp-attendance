<div class="wrap erp-hr-attendance">
    <h2>
        <?php _e( 'Attendance', 'erp-attendance' ); ?> <a href="<?php echo admin_url( 'admin.php?page=erp-new-attendance' ); ?>" class="add-new-h2"><?php _e( 'Add New', 'erp-attendance' ); ?></a>
    </h2>

    <form method="get">
        <input type="hidden" name="page" value="erp-hr-attendance">

        <?php
        $attendance = new \WeDevs\ERP\HRM\Attendance\Attendance();
        $attendance->prepare_items();
        $attendance->display();
        ?>
    </form>
</div>