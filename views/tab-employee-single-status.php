<?php
    global $wpdb;

    $user_id              = isset( $_REQUEST['id'] ) ? $_REQUEST['id'] : '';
    $selected_query_time  = isset( $_REQUEST['query_time'] ) ? $_REQUEST['query_time'] : 'this_month';
    $duration             = erp_att_get_start_end_date( $selected_query_time );
    $attendance_record    = erp_att_get_attendance_by_daterange_by_employee( $user_id, $duration['start'], $duration['end'], 'second' );
    $periods              = calculate_date_period( $duration['start'], $duration['end'] );
    $is_shift_enabled     = is_shift_enabled();
    $office_time          = erp_att_get_office_time();
    $grace_times          = erp_att_get_grace_times();
    $query_times          = erp_att_get_query_times();
    $att_data             = [];
    $checkin_array        = [];
    $checkout_array       = [];
    $lowest_checkin       = 0;
    $highest_checkout     = 0;
    $worktime_total_raw   = 0;
    $worktime_total       = 0;
    $worktime_average_raw = 0;
    $worktime_average     = 0;
    $tick_size            = 0;
    $shift_start          = 0;
    $shift_end            = 0;
    $count_working_days   = 0;
    $count_present        = 0;
    $count_holidays       = 0;
    $count_leave          = 0;
    $count_absent         = 0;
    $count_late           = 0;
    $count_early_left     = 0;
    $average_checkin      = 0;
    $average_checkout     = 0;

    foreach ( $attendance_record['attendance'] as $single ) {
        if ( $single['checkin'] && $single['checkout'] ) {
            $checkin_array[]  = strtotime( $single['checkin'] );
            $checkout_array[] = strtotime( $single['checkout'] );
        }
    }

    $worktime_total_raw = $attendance_record['total_length'];
    $worktime_total     = erp_att_second_to_hour_min( $worktime_total_raw );

    if ( $checkin_array && $checkout_array ) {

        $lowest_checkin    = ( min( $checkin_array ) - strtotime( '00:00:00' ) );
        $highest_checkout  = ( max( $checkout_array ) - strtotime( '00:00:00' ) );
        $average_checkin   = ( array_sum( $checkin_array ) / count( $checkin_array ) ) - strtotime( '00:00:00' );
        $average_checkout  = ( array_sum( $checkout_array ) / count( $checkout_array ) ) - strtotime( '00:00:00' );
    }

    foreach ( $periods as $period ) {

        $date        = $period->format( 'Y-m-d' );
        $leave_exist = erp_hrm_is_leave_recored_exist_between_date( $date, $date, $user_id );

        if ( $leave_exist ) {
            $att_data[$date] = 'leave';
            $count_leave++;
            continue;
        }

        $valid_work_day = erp_hr_get_work_days_between_dates( $date, $date );

        if ( !$valid_work_day['days'][0]['count'] ) {
            $att_data[$date] = 'holiday';
            $count_holidays++;
            continue;
        }

        foreach ( $attendance_record['attendance'] as $single ) {

            if ( $date == $single['date'] && 'no' == $single['present'] ) {
                $att_data[$date] = 'absent';
                $count_absent++;
                continue;
            }

            if ( $date == $single['date'] && 'yes' == $single['present'] ) {

                $count_present++;

//                if ( !$single['checkout'] ) {
//                    $att_data[$date] = 'error';
//                    continue 2;
//                }

                $count_working_days++;
                $timestamp_checkin  = strtotime( $single['checkin'] );
                $timestamp_checkout = strtotime( $single['checkout'] );

                $unix_date = strtotime( $date );

                if ( ! $is_shift_enabled ) {

                    $shift_start = strtotime( $office_time['starts'] );
                    $shift_end   = strtotime( $office_time['ends'] );

                } else {

                    $shift_start = strtotime( $single['shift_start_time'] );
                    $shift_end   = strtotime( $single['shift_end_time'] );
                }

                $result = calculate_work_details_by_date( $shift_start, $shift_end, $single['checkin'], $single['checkout'], $grace_times );

                if ( $result['late'] ) {
                    $count_late++;
                }

                if ( $result['early_left'] ) {
                    $count_early_left++;
                }

                $primary_data = [
                    'checkin'            => ( $timestamp_checkin - strtotime('00:00:00') ) - $result['late'],
                    'early_entry'        => $result['early_entry'],
                    'late'               => $result['late'],
                    'worktime'           => $timestamp_checkout - ( $timestamp_checkin + $result['early_entry'] + $result['extra_time'] ),
                    'early_left'         => $result['early_left'],
                    'extra_time'         => $result['extra_time'],
                    'timestamp_checkin'  => $timestamp_checkin,
                    'timestamp_checkout' => $timestamp_checkout,
                    'shift_title'        => $single['shift_title']
                ];

                if( isset( $att_data[$date] ) && is_array( $att_data[$date] ) ) {
                    array_push( $att_data[$date], $primary_data );
                } else {
                    $att_data[$date] = [];
                    array_push( $att_data[$date], $primary_data );
                }

                if( is_shift_enabled() ) {
                    continue;
                } else {
                    continue 2;
                }
            }

            if( ! is_shift_enabled() ) {
                $att_data[$date] = 'absent';
            }
        }
    }

    if ( $count_working_days ) {

        if ( $count_working_days !== $count_leave ) {
            $worktime_average_raw = absint( $worktime_total_raw / ( $count_working_days - $count_leave ) );
        } else {
            $worktime_average_raw = absint( $worktime_total_raw / $count_working_days );
        }
    }

    $worktime_average     = erp_att_second_to_hour_min( $worktime_average_raw );
    $checkin              = [];
    $early_entry          = [];
    $late                 = [];
    $worktime             = [];
    $early_left           = [];
    $extra_time           = [];
    $holiday              = [];
    $leave                = [];

    $second_shift_start = $lowest_checkin;
    $second_shift_end   = $highest_checkout - $second_shift_start;

    foreach ( $att_data as $date => $data ) {

        $js_date = strtotime( $date ) * 1000;

        if ( is_array( $data ) ) {
            foreach( $data as $data_single ) {

                $checkin[]     = [ $js_date, $data_single['checkin'] * 1000 ];
                $early_entry[] = [ $js_date, $data_single['early_entry'] * 1000 ];
                $late[]        = [ $js_date, $data_single['late'] * 1000 ];
                $worktime[]    = [ $js_date, $data_single['worktime'] * 1000 ];
                $early_left[]  = [ $js_date, $data_single['early_left'] * 1000 ];
                $extra_time[]  = [ $js_date, $data_single['extra_time'] * 1000 ];
                $holiday[]     = [ $js_date, 0 ];
                $leave[]       = [ $js_date, 0 ];
                $absent[]      = [ $js_date, 0 ];
            }
            continue;
        }

        if ( 'absent' == $data ) {
            $checkin[]     = [ $js_date, $second_shift_start * 1000 ];
            $early_entry[] = [ $js_date, 0 ];
            $late[]        = [ $js_date, 0 ];
            $worktime[]    = [ $js_date, 0 ];
            $early_left[]  = [ $js_date, 0 ];
            $extra_time[]  = [ $js_date, 0 ];
            $holiday[]     = [ $js_date, 0 ];
            $leave[]       = [ $js_date, 0 ];
            $absent[]      = [ $js_date, $second_shift_end * 1000 ];
        }

        if ( 'holiday' == $data ) {
            $checkin[]     = [ $js_date, $second_shift_start * 1000 ];
            $early_entry[] = [ $js_date, 0 ];
            $late[]        = [ $js_date, 0 ];
            $worktime[]    = [ $js_date, 0 ];
            $early_left[]  = [ $js_date, 0 ];
            $extra_time[]  = [ $js_date, 0 ];
            $holiday[]     = [ $js_date, $second_shift_end * 1000 ];
            $leave[]       = [ $js_date, 0 ];
            $absent[]      = [ $js_date,0 ];
        }

        if ( 'leave' == $data ) {
            $checkin[]     = [ $js_date, $second_shift_start * 1000 ];
            $early_entry[] = [ $js_date, 0 ];
            $late[]        = [ $js_date, 0 ];
            $worktime[]    = [ $js_date, 0 ];
            $early_left[]  = [ $js_date, 0 ];
            $extra_time[]  = [ $js_date, 0 ];
            $holiday[]     = [ $js_date, 0 ];
            $leave[]       = [ $js_date, $second_shift_end * 1000 ];
            $absent[]      = [ $js_date, 0 ];
        }
    }

    if ( 'this_month' == $selected_query_time || 'last_month' == $selected_query_time ) {
        $tick_size = json_encode( [1, "day"] );
    }
    ?>
    <script>
        ;
        (function($){
            $(document).ready(function() {
                var resdata = [
                    {
                        data: <?php echo json_encode($checkin); ?>,
                        color: '#FAFAFA'
                    },
                    {
                        label: "<?php _e( 'Early Entry', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode($early_entry); ?>,
                        color: '#558dd6'
                    },
                                        {
                        label: "<?php _e( 'Late', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode($late); ?>,
                        color: '#ff3000'
                    },
                    {
                        label: "<?php _e( 'Worktime', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $worktime ); ?>,
                        color: '#1e487b',
                    },
                    {
                        label: "<?php _e( 'Early Left', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $early_left ); ?>,
                        color: '#ff3000',
                    },
                    {
                        label: "<?php _e( 'Extra Time', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $extra_time ); ?>,
                        color: '#558dd6',
                    },
                    {
                        label: "<?php _e( 'Absent', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $absent ); ?>,
                        color: '#dfe0da',
                    },
                    {
                        label: "<?php _e( 'Holiday', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $holiday ); ?>,
                        color: '#dfe0da',
                    },
                    {
                        label: "<?php _e( 'Leave', 'erp-attendance' ); ?>",
                        data: <?php echo json_encode( $leave ); ?>,
                        color: '#dfe0da',
                    }
                ];

                $.plot('#emp-single-att-stacked-chart', resdata, {
                    series: {
                        //lines: { show: true },
                        // points: {
                        //     show: true,
                        //     radius: 4
                        // },
                        bars: {
                            show: true,
                            barWidth: 18 * 3600000,
                            fill: 1,
                            lineWidth: 0,
                        },

                        stack: true
                    },
                    bars: {
                        align: 'center'
                    },
                    valueLabels: {

                        show: true
                    },
                    yaxis:{
                        axisLabel: "Time",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 14,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10,
                        mode: "time",
                        tickSize: [1, "hour"],
                        twelveHourClock: true,
                        min: <?php echo $lowest_checkin; ?> * 1000 - 1800000,
                        max: <?php echo $highest_checkout; ?> * 1000 + 1800000,
                        tickColor: '#eee'
                        // min: 0,
                        // max: 16,
                        // tickSize: 1,
                        // tickColor: '#eee'
                    },
                    xaxis: {
                        axisLabel: "Days",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 14,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10,
                        mode: "time",
                        tickSize: <?php echo $tick_size; ?>,
                        // rotateTicks: 130,
                    },
                    grid: {
                      hoverable: true,
                      clickable: true,
                      borderColor: '#000',
                      borderWidth: {
                        left: 2,
                        bottom: 2,
                        right: 2,
                        top: 2,
                      },
                      backgroundColor: { colors: ["#ffffff", "#F9FBFC"] }
                    },
                    legend: {
                        position: 'ne',
                        show: true,
                        //labelBoxBorderColor: '#FF5722',
                        backgroundColor: '#fff'
                    },

                } );
                $("#emp-single-att-stacked-chart").UseTooltip();


            });

            var previousPoint = null, previousLabel = null;

            $.fn.UseTooltip = function () {
                $(this).bind("plothover", function (event, pos, item) {

                    if (item) {

                        if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                            previousPoint = item.dataIndex;
                            previousLabel = item.series.label;
                            $("#tooltip").remove();

                            var date = new Date(item.datapoint[0]).toString().split(' ').splice(1, 3).join(' ');
                            var data1 = item.datapoint[1];
                            var data2 = item.datapoint[2];
                            var label = item.series.label;
                            var color = item.series.color;

                            if ( undefined != label ) {
                                showTooltip(item.pageX,
                                item.pageY,
                                color,
                                "<?php _e(' Date :', 'erp-attendance' ); ?><strong>" + date + "</strong><br>"+ label +" :<strong>" + calculateTime( data1, data2, label ) + "</strong>");
                            }
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            };

            function showTooltip(x, y, color, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 120,
                    border: '2px solid ' + color,
                    padding: '3px',
                    'font-size': '9px',
                    'border-radius': '5px',
                    'background-color': '#fff',
                    'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                    opacity: 0.9
                }).appendTo("body").fadeIn(200);
            }

            function calculateTime( data1, data2, label ) {

                var totalSec = (data1 / 1000) - (data2 / 1000);
                var hours    = parseInt( totalSec / 3600 ) % 24;
                var minutes  = parseInt( totalSec / 60 ) % 60;
                var result   = (hours < 10 ? "" + hours : hours) + "h " + (minutes < 10 ? "0" + minutes : minutes) + "m";
                return result;
            }

        })(jQuery);

    </script>
<div class="wrap">
    <div id="att-status-single-emp" style="width:90%;">
        <div class="att-query-form">
            <form method="get">
                <input type="hidden" name="page" value="erp-hr-employee">
                <input type="hidden" name="tab" value="attendance">
                <input type="hidden" name="action" value="view">
                <input type="hidden" name="id" value=<?php echo $user_id ?>>
                <select name="query_time">
                <?php
                    foreach ( $query_times as $key => $value ) {
                        echo '<option value="' . $key . '"' . selected( $selected_query_time, $key ) . '>' . $value . '</option>' ;

                    }
                ?>
                </select>
                <?php wp_nonce_field( 'epr-attendance-filter' ); ?>
                <button type="submit" class="button-secondary" name="filter_attendance" value="filter_attendance"><?php _e( 'Filter', 'erp' ); ?></button>
            </form>
        </div>
    	<div id="emp-single-att-stacked-chart" style="width:100%;height:600px;"></div>

        <div class="att-summary-print">
            <button class="button-primary"><i class="fa fa-print"></i>&nbsp;<?php _e( 'Print Summary', 'erp-attendance' ); ?></button>
        </div>

        <div class="postbox leads-actions">
            <div class="handlediv" title="<?php _e( 'Summary', 'erp-attendance' ); ?>"><br></div>
            <h3 class="hndle"><span><?php _e( 'Summary', 'erp-attendance' ); ?></span></h3>
            <div class="inside">
                <ul class="erp-list two-col separated">
                    <li><?php erp_print_key_value( __( 'Total Work Time', 'erp-attendance' ), $worktime_total ); ?></li>
                    <li><?php erp_print_key_value( __( 'Average Work Time', 'erp-attendance' ), $worktime_average ); ?></li>
                    <li>
                        <?php
                            is_shift_enabled() ? erp_print_key_value( __( 'Total Working Shifts', 'erp-attendance' ), $count_working_days ) : erp_print_key_value( __( 'Total Working Days', 'erp-attendance' ), $count_working_days );
                        ?>
                    </li>
                    <?php
                        if( ! is_shift_enabled() ) {
                            erp_print_key_value( __( '<li>Average Checkin</li>', 'erp-attendance' ), erp_att_second_to_hour_min($average_checkin, ':') );
                            erp_print_key_value( __( '<li>Average Checkout</li>', 'erp-attendance' ), erp_att_second_to_hour_min($average_checkout, ':') );
                        }
                    ?>
                    <li><?php erp_print_key_value( __( 'Total Leave', 'erp-attendance' ), $count_leave ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Holiday', 'erp-attendance' ), $count_holidays ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Present', 'erp-attendance' ), $count_present ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Absent', 'erp-attendance' ), $count_absent ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Late', 'erp-attendance' ), $count_late ); ?></li>
                    <li><?php erp_print_key_value( __( 'Total Early Left', 'erp-attendance' ), $count_early_left ); ?></li>
                </ul>
            </div>
        </div><!-- .postbox -->

        <div id="employee-attendance-table">
            <table class="widefat striped">
                <thead>
                    <tr>
                        <th><?php _e( 'Date', 'erp-attendance' ); ?></th>
                        <?php
                            if( is_shift_enabled() ) {
                                _e( '<th>Shift</th>', 'erp-attendance' );
                            }
                        ?>
                        <th><?php _e( 'Status', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Checkin', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Checkout', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Time Woked', 'erp-attendance' ); ?></th>
                        <th><?php _e( 'Comment', 'erp-attendance' ); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ( $att_data as $date => $single ) {

                            if( is_future_date( $date ) ) {
                                continue;
                            }

                            if ( 'leave' == $single ) {
                                echo '<tr>';
                                echo '<td>' . $date . '</td>';
                                if ( is_shift_enabled() ) {
                                    echo '<td>&mdash;</td>';
                                }
                                echo '<td>' . __( 'Leave', 'erp-attendance' ) . '</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '</tr>';
                            }
                            else if ( 'holiday' == $single ) {
                                echo '<tr>';
                                echo '<td>' . $date . '</td>';
                                if ( is_shift_enabled() ) {
                                    echo '<td>&mdash;</td>';
                                }
                                echo '<td><div class="att-color-box att-orange"></div>&nbsp;' . __( 'Holiday', 'erp-attendance' ) . '</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '</tr>';
                            }
                            else if ( 'error' == $single ) {
                                echo '<tr>';
                                echo '<td>' . $date . '</td>';
                                if ( is_shift_enabled() ) {
                                    echo '<td>&mdash;</td>';
                                }
                                echo '<td>' . __( 'Error', 'erp-attendance' ) . '</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '</tr>';
                            }
                            else if ( 'absent' == $single ) {
                                echo '<tr>';
                                echo '<td>' . $date . '</td>';
                                if ( is_shift_enabled() ) {
                                    echo '<td>&mdash;</td>';
                                }
                                echo '<td><div class="att-color-box att-red"></div>&nbsp;' . __( 'Absent', 'erp-attendance' ) . '</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '<td>&mdash;</td>';
                                echo '</tr>';
                            } else {

                                foreach( $single as $partial ) {
                                    echo '<tr>';
                                    echo '<td>' . $date . '</td>';
                                    if ( is_shift_enabled() ) {
                                        echo '<td>'. $partial['shift_title'].'</td>';
                                    }
                                    echo '<td><div class="att-color-box att-green"></div>&nbsp;' . __( 'Present', 'erp-attendance' ) . '</td>';
                                    echo '<td>' . date( 'H:i:s', $partial['timestamp_checkin'] ) . '</td>';
                                    echo '<td>' . date( 'H:i:s', $partial['timestamp_checkout'] ) . '</td>';
                                    echo '<td>' . erp_hr_calculate_working_time( date( 'H:i:s', $partial['timestamp_checkin'] ), date( 'H:i:s', $partial['timestamp_checkout'] ) ) . '</td>';

                                    if ( $partial['late'] ) {
                                        echo '<td>' . __( 'Late', 'erp-attendance' ) . '</td>';
                                    }
                                    else if ( $partial['early_left'] ) {
                                        echo '<td>' . __( 'Early Left', 'erp-attendance' ) . '</td>';
                                    }
                                    else if ( $partial['early_entry'] || $partial['extra_time'] ) {
                                        echo '<td>' . __( 'Extra Time', 'erp-attendance' ) . '</td>';
                                    } else {
                                        echo '<td>' . __( 'Normal', 'erp-attendance' ) . '</td>';
                                    }

                                    echo '</tr>';
                                }

                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>