<div class="erp-att-self-service-widget" v-cloak>
    <div class="clock-container">
	    <div class="self-service-clock">{{ digital_clock }}</div>
    </div>

    <template v-if="is_shift_enabled">
        <div v-if="noShiftAssigned" class="server-message text-center">
            <p>{{ i18n.noShiftAssigned }}</p>
        </div>

        <div v-else class="shift-dorpdown-container text-center">
            <table class="erp-att-shift-info">
                <tbody>
                    <tr>
                        <td class="text-left">
                            <?php _e( 'Shift', 'erp-attendance' ); ?>
                        </td>
                        <td class="text-left">
                            {{ attendance.title }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-left">
                            <?php _e( 'Shift Time' ) ?>
                        </td>
                        <td class="text-left">
                            {{ shift_time }}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-left">
                            <?php _e( 'Server Time' ) ?>
                        </td>
                        <td class="text-left">
                            {{ server_time }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </template>

    <div class="checkin-time-container">
        <div class="self-service-checkin-time"></div>
        <div class="self-service-checkout-time"></div>
    </div>

    <div v-if="showButtons" class="checkin-button-container">
        <button
            :disabled="isCheckinBtnDisabled"
            @click="saveAttendance('checkin')"
            class="button button-primary"
            class="self-service-checkin-button"
        >{{ i18n.checkin }}</button>

        <button
            :disabled="attendance.checkout || doingAjax"
            @click="saveAttendance('checkout')"
            class="button button-primary"
            class="self-service-checkout-button"
        >{{ i18n.checkout }}</button>
    </div>
</div>