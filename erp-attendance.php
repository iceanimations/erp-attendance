<?php
/**
 * Plugin Name: WP ERP - Attendance Management
 * Description: Employee Attendance Add-On for WP ERP
 * Plugin URI: http://wperp.com/downloads/attendance
 * Author: weDevs
 * Author URI: http://wedevs.com
 * Version: 1.1.0
 * License: GPL2
 * Text Domain: erp-attendance
 * Domain Path: languages
 *
 * Copyright (c) 2016 weDevs (email: info@wperp.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( !defined( 'ABSPATH' ) ) exit;

class WeDevs_ERP_HR_Attendance {

    /**
     * Add-on Version
     *
     * @var  string
     */
    public $version = '1.1.0';

    /**
     * Initializes the WeDevs_ERP_HR_Attendance class
     *
     * Checks for an existing WeDevs_ERP_HR_Attendance instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {

        static $instance = false;

        if ( ! $instance ) {
            $instance = new WeDevs_ERP_HR_Attendance();
        }

        return $instance;
    }

    /**
     * Constructor for the WeDevs_ERP_HR_Attendance class
     *
     * Sets up all the appropriate hooks and actions
     */
    public function __construct() {
        //Define constants
        $this->define_constants();

        include WPERP_ATTEND_INCLUDES . '/class-install.php';

        add_action( 'erp_loaded', [ $this, 'plugin_init' ] );
    }

    /**
     * Execute if ERP is installed
     *
     * @return null
     */
    public function plugin_init() {
        // Include files
        $this->includes();

        // Instantiate classes
        $this->init_classes();

        // Init action hooks
        $this->init_actions();

        // Init filter hooks
        $this->init_filters();
    }

    /**
     * Define Add-on constants
     *
     * @return void
     */
    public function define_constants() {
        if ( defined( 'WPERP_ATTEND_VERSION' ) ) {
            return;
        }

        define( 'WPERP_ATTEND_VERSION', $this->version );
        define( 'WPERP_ATTEND_FILE', __FILE__ );
        define( 'WPERP_ATTEND_PATH', dirname( WPERP_ATTEND_FILE ) );
        define( 'WPERP_ATTEND_INCLUDES', WPERP_ATTEND_PATH . '/includes' );
        define( 'WPERP_ATTEND_URL', plugins_url( '', WPERP_ATTEND_FILE ) );
        define( 'WPERP_ATTEND_ASSETS', WPERP_ATTEND_URL . '/assets' );
        define( 'WPERP_ATTEND_VIEWS', WPERP_ATTEND_PATH . '/views' );
        define( 'WPERP_ATTEND_JS_TMPL', WPERP_ATTEND_VIEWS . '/js-templates' );
    }

    /**
     * Include the required files
     *
     * @return void
     */
    public function includes() {
        include_once WPERP_ATTEND_INCLUDES . '/model/attendance.php';
        include_once WPERP_ATTEND_INCLUDES . '/model/employee.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-attendance.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-attendance-single.php';
        include_once WPERP_ATTEND_INCLUDES . '/functions-attendance.php';
        include_once WPERP_ATTEND_INCLUDES . '/functions-shift.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-ajax.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-form-handler.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-widgets.php';
        include_once WPERP_ATTEND_INCLUDES . '/class-updates.php';
    }

    /**
     * Registers all the scripts to ERP init
     *
     * @return void
     */
    public function register_scripts( $hook ) {
        wp_register_script( 'erp-att-sortablejs', WPERP_ATTEND_ASSETS . '/js/sortable.min.js', [], WPERP_ATTEND_VERSION, true );
        wp_register_script( 'erp-att-vuedraggable', WPERP_ATTEND_ASSETS . '/js/vuedragablefor.min.js', [], WPERP_ATTEND_VERSION, true );

        if ( 'attendance_page_erp-hr-shifts' === $hook ) {
            wp_register_script( 'erp-att-lodash', WPERP_ATTEND_ASSETS . '/js/lodash.min.js', [], WPERP_ATTEND_VERSION, true );
        }

        // Enqueue jQuery timepicker
        wp_enqueue_style( 'erp-timepicker' );

        // Enqueue main css style
        wp_enqueue_style( 'erp-attendance-main-style', WPERP_ATTEND_ASSETS . '/css/attendance.css' );

        if ( ! is_admin() ) {
            wp_enqueue_style( 'erp-attendance-frontend', WPERP_ATTEND_ASSETS . '/css/erp-attendance-frontend.css', ['erp-attendance-main-style'], WPERP_ATTEND_VERSION );
        }

        // Register jQuery flot stack chart
        wp_register_script( 'erp-att-flot-stack', WPERP_ATTEND_ASSETS . '/js/jquery.flot.stack.js', ['erp-flotchart'], WPERP_ATTEND_VERSION, true );

        // Register jQuery flot chart tick rotator
        wp_register_script( 'erp-att-flot-tickrotator', WPERP_ATTEND_ASSETS . '/js/jquery.flot.tickrotator.js', ['erp-flotchart'], WPERP_ATTEND_VERSION, true );

        // Enqueue main js script
        wp_enqueue_script( 'erp-attendance-main-script', WPERP_ATTEND_ASSETS . '/js/attendance.js', [
            'jquery',
            'erp-momentjs',
            'jquery-ui-datepicker',
            'erp-timepicker',
            'erp-flotchart',
            'erp-flotchart-pie',
            'erp-att-flot-stack',
            'erp-att-flot-tickrotator',
            'erp-flotchart-time',
            'erp-flotchart-tooltip',
            'erp-flotchart-axislables',
            'erp-vuejs'
        ], WPERP_ATTEND_VERSION, true );

        wp_enqueue_script( 'erp-shift-script', WPERP_ATTEND_ASSETS . '/js/shift.js', ['erp-attendance-main-script', 'erp-att-sortablejs', 'erp-att-lodash', 'erp-att-vuedraggable'], WPERP_ATTEND_VERSION, true );

        wp_enqueue_style( 'erp-timepicker' );

        $localize_scripts = [
            'scriptDebug'          => defined( 'SCRIPT_DEBUG' ) ? SCRIPT_DEBUG : false,
            'att_main_url'         => admin_url( 'admin.php?page=erp-hr-attendance' ),
            'current_date'         => current_time( 'Y-m-d' ),
            'utc_offset'           => get_option( 'gmt_offset' ),
            'nonce'                => wp_create_nonce( 'wp-erp-attendance' ),
            'hook'                 => $hook,
            'shift_delete_warning' => __( "This shift and related attendance record will be deleted permanently and can't be undone. Are you sure?", 'erp-attendance' ),
            'popup' => [
               'attendanceNew'          => __( 'New Attendance', 'erp-attendance' ),
               'attendanceNewSubmit'    => __( 'Submit Attendance', 'erp-attendance' ),
               'attendanceEdit'         => __( 'Edit Attendance', 'erp-attendance' ),
               'attendanceEditSubmit'   => __( 'Save Changes', 'erp-attendance' ),
               'attendanceImport'       => __( 'Import Attendance', 'erp-attendance' ),
               'attendanceImportSubmit' => __( 'Import', 'erp-attendance' ),
            ],
            'alert' => [
                'somethingWrong'        => __( 'Something went wrong', 'erp-attendance' )
            ],
            'selfService' => [
                'checkoutMsg'           => __( 'Do you want to checkout?', 'erp-attendance' )
            ],
            'empty_shift' => get_empty_shift(),
            'office_time' => erp_att_get_office_time()
        ];

        if ( 'hr-management_page_erp-hr-employee' == $hook ) {

            $localize_scripts['user_id'] = isset( $_REQUEST['id'] ) ? $_REQUEST['id'] : '';
        }

        if( isset( $_REQUEST['page'] ) && 'erp-edit-attendance' == $_REQUEST['page'] ) {
            $localize_scripts['current_date'] = esc_attr( $_REQUEST['edit_date'] );
        }
        // Localize scripts
        wp_localize_script( 'erp-attendance-main-script', 'wpErpAttendance', $localize_scripts );
    }

    /**
     * Initialize the classes
     *
     * @since 1.0
     *
     * @return void
     */
    public function init_classes() {

        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            new WeDevs\ERP\HRM\Attendance\Attendance_Ajax_Handler();
        }

        if ( is_admin() && class_exists( '\WeDevs\ERP\License' ) ) {
            new \WeDevs\ERP\License( __FILE__, 'Attendance Management', $this->version, 'weDevs' );
        }

        // Widget instance
        new \WeDevs\ERP\HRM\Attendance\Widgets();

        // updater class
        new \WeDevs\ERP\HRM\Attendance\Updates();
    }

    /**
     * Initializes action hooks to ERP
     *
     * @return void
     */
    public function init_actions() {
        // Load localize textdomain
        add_action( 'init', [ $this, 'localization_setup' ] );

        // Add attendance menu
        add_action( 'admin_menu', [ $this, 'add_menu' ], 99 );

        // Load JS Templates
        add_action( 'admin_footer', [ $this, 'admin_js_templates' ] );

        // Enqueue script files
        add_action( 'admin_enqueue_scripts', [ $this, 'register_scripts' ] );

        // Enqueue script files
        add_action( 'template_redirect', [ $this, 'register_scripts' ] );

        // Adds a tab in a single employee page
        add_action( 'erp_hr_employee_single_tabs', [ $this, 'erp_hr_employee_single_attendance_callback' ] );

        // Attendance table bulk actions
        add_action( 'load-hr-management_page_erp-hr-attendance', [ $this, 'attendance_bulk_action' ] );

        // Add shift table to erp settings page
//        add_action( 'erp_admin_field_shift_table', [ $this, 'add_shift_table' ] );
    }

    /**
     * Initializes action hooks to ERP
     *
     * @return void
     */
    public function init_filters() {
        // Add a section to HR Settings
        add_filter( 'erp_settings_hr_sections', [ $this, 'add_att_sections' ] );

        // Add fields to ERP Settings Attendance section
        add_filter( 'erp_settings_hr_section_fields', [ $this, 'add_att_section_fields' ], 10, 2 );

        // Attendance tab in HR Settings
        add_filter( 'erp_hr_settings_tabs', [ $this, 'attendance_settings_page' ] );

        // Adds an option for Attendance report in HR reporting page
        add_filter( 'erp_hr_reports', [ $this, 'attendance_report' ] );

        // Creates a separate report page for attendance report
        add_filter( 'erp_hr_reporting_pages', [ $this, 'attendance_report_page' ], 10, 2 );
    }

    /**
     * Initialize plugin for localization
     *
     * @since 1.0
     *
     * @uses load_plugin_textdomain()
     *
     * @return void
     */
    public function localization_setup() {
        load_plugin_textdomain( 'erp-attendance', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     * Add submenu to HR Management Page
     *
     * @since 1.0
     *
     * @return void
     */
    public function add_menu() {
        add_menu_page( __( 'Attendance', 'erp-attendance' ), __( 'Attendance', 'erp-attendance' ), 'erp_hr_manager', 'erp-hr-attendance', array( $this, 'attendance_main_callback' ), 'dashicons-forms' );
        add_submenu_page( 'erp-hr-attendance', __( 'Shifts', 'erp-attendance' ), __( 'Shifts', 'erp-attendance' ), 'erp_hr_manager', 'erp-hr-shifts', array( $this, 'attendance_manage_shifts' ) );
        add_submenu_page( 'erp-hr-attendance', __( 'Import/Export', 'erp-attendance' ), __( 'Import/Export', 'erp-attendance' ), 'erp_hr_manager', 'erp-shfit-exim', array( $this, 'attendance_import_export' ) );
        add_submenu_page( '', __( 'New Attendance', 'erp-attendance' ), __( 'New Attendance', 'erp-attendance' ), 'erp_hr_manager', 'erp-new-attendance', array( $this, 'new_attendance' ) );
        add_submenu_page( '', __( 'Edit Attendance', 'erp-attendance' ), __( 'Edit Attendance', 'erp-attendance' ), 'erp_hr_manager', 'erp-edit-attendance', array( $this, 'edit_attendance' ) );
    }

    /**
     * Attendance Main Page
     *
     * @since 1.0
     *
     * @return void
     */
    public function attendance_main_callback() {

        $action = isset( $_GET['q'] ) ? $_GET['q'] : 'list';
        $id     = isset( $_GET['id'] ) ? $_GET['id'] : 0;

        switch ($action) {

            case 'view':
                $template = WPERP_ATTEND_VIEWS . '/attendance-single.php';
                break;

            default:
                $template = WPERP_ATTEND_VIEWS . '/attendance.php';
                break;
        }

        if ( file_exists( $template ) ) {

            include $template;
        }
    }

    /**
     * Attendace Shifts Page
     *
     * @since 1.2
     *
     * @return void
     */
    public function attendance_manage_shifts() {
        include WPERP_ATTEND_VIEWS . '/manage-shifts.php';
    }

    /**
     * Attendance import export
     *
     * @since 1.2
     *
     * @return void
     */
    public function attendance_import_export() {
        include WPERP_ATTEND_VIEWS . '/import.php';
    }

    /**
     * Include new attendance page
     */
    public function new_attendance() {
        include WPERP_ATTEND_VIEWS . '/new-attendance.php';
    }

    /**
     * Include edit attendance page
     */
    public function edit_attendance() {

        include WPERP_ATTEND_VIEWS . '/edit-attendance.php';
    }

    /**
     * Load JS Templates to appropriate page
     *
     * @since 1.0
     *
     * @return null
     */
    public function admin_js_templates() {

        global $current_screen;

        switch ( $current_screen->base ) {

            case 'toplevel_page_erp-hr-attendance':
                erp_get_js_template( WPERP_ATTEND_JS_TMPL . '/attendance-new.php', 'erp-attendance-new' );
//                erp_get_js_template( WPERP_ATTEND_JS_TMPL . '/attendance-import.php', 'erp-attendance-import' );
                break;

//            case 'erp-settings_page_erp-settings':
//                erp_get_js_template( WPERP_ATTEND_JS_TMPL . '/shift-new.php', 'erp-shift-new' );

            default:
                break;
        }
   }

    /**
     * fucntion for Attendage Setting Tab
     *
     * @since 1.0
     *
     * @return mixed
     */
    public function attendance_settings_page( $tabs ) {

            $tabs['attendance'] = [
                'title'    => __( 'Attendance', 'erp-attendance' ),
                'callback' => array( $this, 'attendance_tab' )
            ];

            return $tabs;
    }

    /**
     * Attendance Tab in HR Settings
     */
    public function attendance_tab() {
        include WPERP_ATTEND_VIEWS . '/tab-hr-settings-attendance.php';
    }

    /**
     *
     */
    public function erp_hr_employee_single_attendance_callback( $tabs ) {

        $tabs[ 'attendance' ] = [
            'title'    => __( 'Attendance', 'erp-attendance' ),
            'callback' => [$this, 'erp_hr_employee_single_attendance_tab']
        ];

        return $tabs;
    }

    /**
     *
     */
    public function erp_hr_employee_single_attendance_tab() {
        include WPERP_ATTEND_VIEWS . '/tab-employee-single-status.php';
    }

    /**
     * Check is current page actions
     *
     * @since 0.1
     *
     * @param  integer $page_id
     * @param  integer $bulk_action
     *
     * @return boolean
     */
    public function verify_current_page_screen( $page_id, $bulk_action ) {

        if ( ! isset( $_REQUEST['_wpnonce'] ) || ! isset( $_GET['page'] ) ) {
            return false;
        }

        if ( $_GET['page'] != $page_id ) {
            return false;
        }

        if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], $bulk_action ) ) {
            return false;
        }

        return true;
    }

    /**
     *
     */
    public function attendance_bulk_action() {

        if ( ! $this->verify_current_page_screen( 'erp-hr-attendance', 'bulk-attendances' ) ) {
            return;
        }

        $attendance_table = new \WeDevs\ERP\HRM\Attendance\Attendance();
        $action           = $attendance_table->current_action();

        if ( $action ) {

            $redirect = remove_query_arg( array( '_wp_http_referer', '_wpnonce' ), wp_unslash( $_SERVER['REQUEST_URI'] ) );

            switch ( $action ) {

                case 'filter_attendance':
                    $redirect = remove_query_arg( array( 'filter_attendance', 'action', 'action2' ), $redirect );
                    wp_redirect( $redirect );
                exit;

                default:
                exit;
            }
        }
    }

    /**
     * Add attendance report to HR reporting page
     *
     * @param  $reports  array
     *
     * @return array
     */
    public function attendance_report( $reports ) {
        $reports['attendance-report'] = [
            'title'       => __( 'Attendance', 'erp-attendance' ),
            'description' => __( 'Reporting on employee attendance', 'erp-attendance' )
        ];

        return $reports;
    }

    /**
     * Creates a separate page for attendance report
     *
     * @return mixed
     */
    public function attendance_report_page( $template, $action ) {

        if ( 'attendance-report' == $action ) {
            $template = WPERP_ATTEND_VIEWS . '/attendance-reporting.php';
        }

        return $template;
    }

    /**
     * Add Attendance Sections to ERP Settings Page
     *
     * @since 1.0
     *
     * @return array
     */
    public function add_att_sections( $sections ) {

        $sections ['attendance'] = __( 'Attendance', 'erp-attendance' );
        return $sections;
    }

    /**
     * Add fields to Attendance Section in ERP Fields
     *
     * @since 1.0
     *
     * @return array
     */
    public function add_att_section_fields( $fields, $section ) {

        if ( 'attendance' == $section ) {

            $fields['attendance'] = [
                [
                    'title' => __( 'Shift Management', 'erp-attendance' ),
                    'type'  => 'shift_table',
                    'id'    => 'shift_table',
                ],
                [
                    'title' => __( 'Grace Time', 'erp-attendance' ),
                    'type'  => 'title',
                    'id'    => 'erp_att_grace'
                ],
                [
                    'title'   => __( 'Grace Before Checkin', 'erp-attendance' ),
                    'type'    => 'text',
                    'id'      => 'grace_before_checkin',
                    'desc'    => '(in minute) this time will not counted as overtime',
                    'default' => 15
                ],
                [
                    'title'   => __( 'Grace After Checkin', 'erp-attendance' ),
                    'type'    => 'text',
                    'id'      => 'grace_after_checkin',
                    'desc'    => '(in minute) this time will not counted as late',
                    'default' => 15
                ],
                [
                    'title'   => __( 'Grace Before Checkout', 'erp-attendance' ),
                    'type'    => 'text',
                    'id'      => 'grace_before_checkout',
                    'desc'    => '(in minute) this time will not counted as early left',
                    'default' => 15
                ],
                [
                    'title'   => __( 'Grace After Checkout', 'erp-attendance' ),
                    'type'    => 'text',
                    'id'      => 'grace_after_checkout',
                    'desc'    => '(in minute) this time will not counted as overtime',
                    'default' => 15
                ],
                [
                    'title' => __( 'Self Attendance', 'erp-attendance' ),
                    'type'  => 'checkbox',
                    'id'    => 'enable_self_att',
                    'desc'  => 'Enable Self Attendance Service for Employees?'
                ],
                [
                    'title' => __( 'Shift Management', 'erp-attendance' ),
                    'type'  => 'checkbox',
                    'id'    => 'enable_shift',
                    'desc'  => 'Enable Shift?'
                ],
            ];

            if ( ! is_shift_enabled() ) {

                array_push( $fields['attendance'],
                    [
                        'title' => __( 'Office Starts', 'erp-attendance' ),
                        'type'  => 'text',
                        'id'    => 'office_starts',
                        'desc'  => '',
                        'class' => 'attendance-time-field',
                        'default' => '10:00 AM'
                    ],
                    [
                        'title' => __( 'Office Ends', 'erp-attendance' ),
                        'type'  => 'text',
                        'id'    => 'office_ends',
                        'desc'  => '',
                        'class' => 'attendance-time-field',
                        'default' => '06:00 PM'
                    ]
                );
            }

            $fields['attendance'][] = [
            'type'  => 'sectionend',
            'id'    => 'script_styling_options'
            ];

        }

        return $fields;
    }
}

WeDevs_ERP_HR_Attendance::init();

