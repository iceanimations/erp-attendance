<?php
namespace WeDevs\ERP\HRM\Attendance;

class Attendance_Ajax_Handler {

    public function __construct() {
        add_action( 'wp_ajax_erp_att_save_self_attendance', [ $this, 'save_self_attendance' ] );
        add_action( 'wp_ajax_erp_att_get_attendance_data', [ $this, 'get_attendance_data'] );
        add_action( 'wp_ajax_attendance-stack-single-get', [ $this, 'attendance_get_single_stack_chart_data'] );
        add_action( 'wp_ajax_erp_get_att_by_date', [ $this, 'get_attendance_by_date'] );
        add_action( 'wp_ajax_erp_att_save_hr_input', [ $this, 'attendance_save_by_hr'] );
        add_action( 'wp_ajax_erp_att_get_employee_attendance_data', [ $this, 'get_employee_attendance_data'] );
        add_action( 'wp_ajax_erp_get_att_by_date_for_edit', [ $this, 'get_attendance_by_date_for_edit'] );
        add_action( 'wp_ajax_erp_get_att_weekly_shift_data', [ $this, 'get_weekly_shift_data'] );
        add_action( 'wp_ajax_erp_att_save_weekly_shifts', [ $this, 'save_weekly_shifts'] );
        add_action( 'wp_ajax_erp_att_delete_single_shift', [ $this, 'delete_single_shift'] );
        add_action( 'wp_ajax_erp_att_get_shifts', [ $this, 'get_shift_list'] );
        add_action( 'wp_ajax_erp_att_copy_shifts_to_next_week', [ $this, 'copy_shifts_to_next_week'] );
        add_action( 'wp_ajax_erp_att_bulk_action', [ $this, 'manage_bulk_action'] );
    }

    /**
     * Sorts Time Descend
     * @param  time $a
     * @param  time $b
     * @return mix
     */
    public function _sort_time( $a, $b ) {

        $t1 = strtotime( $a );
        $t2 = strtotime( $b );

        return $t2 - $t1;
    }

    /**
     * Save self attendance data
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function save_self_attendance() {
        if ( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $type = isset( $_POST['type'] ) ? $_POST['type'] : 'checkin';

        $attendance = save_self_attendance( $type );

        if ( ! is_wp_error( $attendance ) )  {
            wp_send_json_success( $attendance );
        } else {
            wp_send_json_error( $attendance->get_error_message() );
        }
    }

    /**
     * Get attendance status data
     *
     * @since 1.0
     *
     * @return void
     */
    public function get_attendance_data() {
        if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $query           = isset( $_GET['query'] ) ? $_GET['query'] : 0;
        $duration        = erp_att_get_start_end_date( $query );
        $is_hr_manager   = current_user_can( erp_hr_get_manager_role() );
        $user_id         = ! $is_hr_manager ? get_current_user_id() : 0;
        $attendance_data = erp_att_get_attendance_data( $duration['start'], $duration['end'], $user_id, true );

        if ( ! empty( $attendance_data ) ) {
            $data = [
                [
                    'label' => __( 'In-time', 'erp-attendance' ),
                    'data' => $attendance_data['proper_checkins']->count(),
                    'color' => '#4CAF50'
                ],
                [
                    'label' => __( 'Late', 'erp-attendance' ),
                    'data' => $attendance_data['late_checkins']->count(),
                    'color' => '#FF9800'
                ],
                [
                    'label' => __( 'Absents', 'erp-attendance' ),
                    'data' => $attendance_data['absents']->count(),
                    'color' => '#F44336'
                ],
            ];

        } else {
            $data = [];
        }


        wp_send_json_success( $data );
    }

    /**
     * Get data for single employee stacked chart
     *
     * @since 1.0
     *
     * @return void
     */
    public function attendance_get_single_stack_chart_data() {

        $attendance = new \WeDevs\ERP\HRM\Models\Attendance();
        $query      = isset( $_REQUEST['query'] ) ? $_REQUEST['query'] : 0;
        $id         = isset( $_REQUEST['user'] ) ? intval( $_REQUEST['user'] ) : 0;
        $duration   = erp_att_get_start_end_date( $query );
        $begin      = new \DateTime( $duration['start'] );
        $end        = new \DateTime( $duration['end'] );
        $end        = $end->modify( '+1 day' );
        $interval   = \DateInterval::createFromDateString( '1 day' );
        $periods    = new \DatePeriod( $begin, $interval, $end );

        foreach ( $periods as $period ) {

            $date              = $period->format( 'Y-m-d' );
            $result            = calculate_worktime_by_date( $date, $id );
            $date_js_timestamp = strtotime( $date ) * 1000;

            $stack[]       = [$date_js_timestamp, isset( $result['shift_start'] ) ? $result['shift_start'] * 1000 : ''];
            $worktime[]    = [$date_js_timestamp, isset( $result['worktime'] ) ? $result['worktime'] * 3600000  : ''];
            $late[]        = [$date_js_timestamp, isset( $result['late'] ) ? $result['late'] * 3600000 : ''];
            $early_left[] = [$date_js_timestamp, isset( $result['early_left'] ) ? $result['early_left'] * 3600000 : ''];
            $overtime[]    = [$date_js_timestamp, isset( $result['overtime'] ) ? $result['overtime'] * 3600000 : ''];
        }

        $data = [
            'stack'       => $stack,
            'worktime'    => $worktime,
            'late'        => $late,
            'early_left' => $early_left,
            'overtime'    => $overtime
        ];

        wp_send_json_success( $data );
    }

    /**
     * Make a report options to HR reporting
     *
     * @return array
     */
    public function attendance_report( $reports ) {

        $report = [
            'title'       => __( 'Attendance Report', 'erp-attendance' ),
            'description' => __( 'Shows a detailed report for employee attendance for different departments', 'erp-attendance' ),
            'slug'        => 'attendance-report'
        ];

        array_push( $reports, $report );

        return $reports;
    }

    /**
     * Get attendance details for HR input
     *
     * @return void
     */
    public function get_attendance_by_date() {

        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $date             = sanitize_text_field( $_REQUEST['date'] );
        $employees        = erp_hr_get_employees( ['number' => -1] );
        $final_attendnace = [];

        if ( is_shift_enabled() ) {
            $attendance = erp_att_get_shift_by_date( $date );

            foreach( $attendance['shifts'] as $att ) {
                foreach( $employees as $employee ) {
                    if( $att['user_id'] == $employee->id ) {
                        array_push( $final_attendnace, [
                            'user_id'       => $employee->id,
                            'employee_id'   => $employee->employee_id,
                            'employee_name' => $employee->first_name . ' ' . $employee->last_name,
                            'shift'         => $att['shift_title'],
                            'present'       => $att['present'],
                            'checkin'       => date( 'H:i', strtotime( $att[ 'shift_start_time' ] ) ),
                            'checkout'      => date( 'H:i', strtotime( $att[ 'shift_end_time' ] ) ),
                            'id'            => $att['id']
                        ] );
                    }
                }
            }

        } else {
            $office_time = erp_att_get_office_time();

            foreach( $employees as $employee ) {
                array_push( $final_attendnace, [
                    'user_id'       => $employee->id,
                    'employee_id'   => $employee->employee_id,
                    'employee_name' => $employee->first_name . ' ' . $employee->last_name,
                    'shift'         => '',
                    'present'       => 'yes',
                    'checkin'       => $office_time[ 'starts' ],
                    'checkout'      => $office_time[ 'ends' ]
                ] );
            }
        }

        wp_send_json_success($final_attendnace);
    }

    /**
     * Save attendance by HR Manager
     *
     * @return void
     */
    public function attendance_save_by_hr() {

        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $date        = sanitize_text_field( $_REQUEST['date'] );
        $attendance  = $_REQUEST['attendance'];
        $office_time = erp_att_get_office_time();

        if( ! $date ) {
            return;
        }

        foreach( $attendance as $att ) {
            if( isset( $att['id'] ) && $att['id'] ) {

                $att['checkin']  = 'yes' == $att['present'] ? $att['checkin'] : null;
                $att['checkout'] = 'yes' == $att['present'] ? $att['checkout'] : null;

                if( ! $att['checkin'] || '00:00' == $att['checkin'] || '00:00:00' == $att['checkin'] ) {
                    $att['present']  = 'no';
                    $att['checkout'] = '00:00';
                }

                erp_att_insert_attendance( $att['id'], $att['present'], $att[ 'checkin' ], $att[ 'checkout' ] );

            } else {

                $att['checkin']  = 'yes' == $att['present'] ? $att['checkin'] : null;
                $att['checkout'] = 'yes' == $att['present'] ? $att['checkout'] : null;
                $shift_id        = erp_att_insert_new_shift( '', $date, $office_time['starts'], $office_time['ends'], $att['user_id'] );

                if( ! $att['checkin'] ) {
                    $att['present'] = 'no';
                }

                if( $shift_id ) {
                    erp_att_insert_attendance( $shift_id, $att['present'], $att[ 'checkin' ], $att[ 'checkout' ] );
                }
            }
        }

        wp_send_json_success($attendance);
    }

    /**
     * Get single attendance by employee
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function get_employee_attendance_data() {
        if ( ! isset( $_GET['_wpnonce'] ) || ! wp_verify_nonce( $_GET['_wpnonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $employee_id      = ! empty( $_GET['employee_id'] ) ? absint( $_GET['employee_id'] ) : get_current_user_id();
        $today            = current_time( 'Y-m-d' );
        $is_shift_enabled = is_shift_enabled() ? true : false;
        $query            = \WeDevs\ERP\HRM\Models\Attendance::where( 'user_id', $employee_id )->where( 'date', $today );

        $attendance = [
            'id'       => 0,
            'title'    => '',
            'checkin'  => '',
            'checkout' => ''
        ];

        $shift_time = '';

        if ( ! $is_shift_enabled ) {
            $query = $query->first();

            if ( $query ) {
                $attendance = [
                    'id'       => $query->id,
                    'title'    => $query->shift_title,
                    'checkin'  => $query->checkin,
                    'checkout' => $query->checkout,
                ];
            }

        } else {
            $query = $query->get();
            $current_time = current_time( 'timestamp' );

            $query->each( function ( $att ) use ( &$attendance, $current_time, &$shift_time ) {
                $start_datetime = new \DateTime( $att->date . ' ' . $att->shift_start_time );
                $end_datetime   = new \DateTime( $att->date . ' ' . $att->shift_end_time );

                if ( $start_datetime->getTimestamp() > $end_datetime->getTimestamp() ) {
                    $end_datetime = $end_datetime->modify( '+1 day' );
                }

                // this will ensure checkin/out buttons will show 15 mins before shift starts upto
                // one hour after shift ends
                $fifteen_min_earlier = $start_datetime->getTimestamp() - (15*60);
                $one_hour_later      = $end_datetime->getTimestamp() + (60*60);

                if ( ( $current_time >= $fifteen_min_earlier ) && ( $current_time <= $one_hour_later ) ) {
                    $attendance = [
                        'id'       => $att->id,
                        'title'    => $att->shift_title,
                        'checkin'  => $att->checkin,
                        'checkout' => $att->checkout,
                    ];
                }

                $shift_starts = date( 'h:i A', strtotime( $att->shift_start_time ) );
                $shift_ends   = date( 'h:i A', strtotime( $att->shift_end_time ) );
                $shift_time   = $shift_starts . ' - ' . $shift_ends;
            } );
        }

        wp_send_json_success( [
            'is_shift_enabled'  => $is_shift_enabled,
            'attendance'        => $attendance,
            'server_time'       => current_time( 'M d h:i A' ),
            'shift_time'        => $shift_time
        ] );
    }


    /**
     * Get attendance details for HR input for Edit
     *
     * @return void
     */
    public function get_attendance_by_date_for_edit() {

        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $date             = sanitize_text_field( $_REQUEST['date'] );
        $employees        = erp_hr_get_employees( [ 'number' => -1 ] );
        $final_attendnace = [];

        $attendance = erp_att_get_attendance_by_date( $date );
        $a = '';

        foreach( $attendance['attendance'] as $att ) {
            foreach( $employees as $employee ) {
                if( $att['user_id'] == $employee->id ) {
                    array_push( $final_attendnace, [
                        'user_id'       => $employee->id,
                        'employee_id'   => $employee->employee_id,
                        'employee_name' => $employee->first_name . ' ' . $employee->last_name,
                        'shift'         => $att[ 'shift_title' ],
                        'present'       => $att[ 'present' ],
                        'checkin'       => $att[ 'checkin' ] ? date( 'H:i', strtotime( $att[ 'checkin' ] ) ) : date( 'H:i', strtotime( $att[ 'shift_start_time' ] ) ),
                        'checkout'      => $att[ 'checkout' ] ? date( 'H:i', strtotime( $att[ 'checkout' ] ) ) : date( 'H:i', strtotime( $att[ 'shift_end_time' ] ) ),
                        'id'            => $att['id']
                    ] );
                }
            }
        }

        wp_send_json_success($final_attendnace);
    }

    /**
     * Get shift data of current week
     *
     * @since 1.2
     *
     * @return void
     */
    public function get_weekly_shift_data() {

        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $dataweek     = isset( $_REQUEST['week'] ) && $_REQUEST['week'] ? $_REQUEST['week'] : 'current';
        $current_date = isset( $_REQUEST['date'] ) && $_REQUEST['date'] ? $_REQUEST['date'] : current_time( 'Y-m-d' );

        if( 'current' == $dataweek ) {
            $date = $current_date;
        }

        if( 'next' == $dataweek ) {
            $date = date( 'Y-m-d', strtotime( $current_date . ' +1 week' ) );
        }

        if( 'previous' == $dataweek ) {
            $date = date( 'Y-m-d', strtotime( $current_date . ' -1 week') );
        }

        $date            = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
        $start_date      = $date->hour(0)->minute(0)->second(0)->startOfWeek()->Format('Y-m-d');
        $end_date        = $date->hour(0)->minute(0)->second(0)->endOfWeek()->Format('Y-m-d');
        $employees       = erp_hr_get_employees( [ 'number' => -1 ] );
        $periods         = calculate_date_period( $start_date, $end_date );
        $employee_shifts = [];

        foreach( $employees as $employee ) {

            $employee_shifts[$employee->id] = [
                'employee_name' => $employee->first_name . ' ' . $employee->last_name,
                'employee_id'   => $employee->employee_id,
                'user_id'       => $employee->id,
                'selected'      => false
            ];

            foreach( $periods as $period ) {

                $date       = $period->format( 'Y-m-d' );
                $shfits_raw = erp_att_get_shift_by_date_by_employee( $date, $employee->id );
                $shifts     = $shfits_raw['shifts'];

                $employee_shifts[$employee->id]['dates'][] = [
//                    'date'   => erp_format_date( $date ),
                    'date'   => $date,
                    'shifts' => $shifts
                ];
            }
        }

        wp_send_json_success( array_values( $employee_shifts ) );
    }

    /**
     * Save shifts data when shift is enabled
     *
     * @return void
     */
    public function save_weekly_shifts() {
        if ( ! isset( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $shift_data     = $_POST['shift_data'];
        $shift_list_raw = $_POST['shift_list'];
        $shift_list     = [];

        foreach ( $shift_list_raw as $shift ) {
            $shift_list[] = [
                'shift_title' => $shift['shift_title'],
                'shift_start_time' => $shift['shift_start_time'],
                'shift_end_time' => $shift['shift_end_time'],
            ];
        }

        update_option( 'erp_shift_list', $shift_list );

        foreach( $shift_data as $employee ) {
            if ( isset( $employee['dates'] ) ) {
                foreach( $employee['dates'] as $date ) {
                    if ( isset( $date['shifts'] ) ) {
                        foreach( $date['shifts'] as $shift ) {
                            erp_att_insert_new_shift( $shift['shift_title'], $date['date'], $shift['shift_start_time'], $shift['shift_end_time'], $employee['user_id'] );
                        }
                    }
                }
            }
        }

        wp_send_json_success( $shift_data );
    }

    /**
     * Delete a single shift
     *
     * @return void
     */
    public function delete_single_shift() {
        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $shift_id = isset( $_REQUEST['shift_id'] ) && $_REQUEST['shift_id'] ? intval( $_REQUEST['shift_id'] ) : '';

        if( $shift_id ) {
            erp_att_delete_shift( $shift_id );
            wp_send_json_success();
        }
    }

    /**
     * Get shift list
     *
     * @return void
     */
    public function get_shift_list() {
        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $shift_list = get_option( 'erp_shift_list', [] );

        wp_send_json_success( $shift_list );
    }

    /**
     * Copy shifts from current week to next week
     *
     * @return void
     */
    public function copy_shifts_to_next_week() {
        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are not allowed' );
        }

        $date              = isset( $_REQUEST['date'] ) && $_REQUEST['date'] ? $_REQUEST['date'] : '';
        $current_week_data = isset( $_REQUEST['current_week_data'] ) && $_REQUEST['current_week_data'] ? $_REQUEST['current_week_data'] : '';

        if( $date && $current_week_data ) {

            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
            $date_next_week = $date->addDays( 7 );
            $start_date      = $date->hour(0)->minute(0)->second(0)->startOfWeek()->Format('Y-m-d');
            $end_date        = $date->hour(0)->minute(0)->second(0)->endOfWeek()->Format('Y-m-d');
            $periods         = calculate_date_period( $start_date, $end_date );
            $dates = [];

            foreach( $periods as $period ) {
                array_push( $dates, $period->format( 'Y-m-d' ) );
            }

            for( $i = 0; $i < count( $dates ); $i++ ) {
                $current_week_data['dates'][$i]['copy_to'] = $dates[$i];
            }

            foreach( $current_week_data['dates'] as $date ) {
                if( isset( $date['shifts'] ) && is_array( $date['shifts'] ) ) {
                    foreach($date['shifts'] as $shift ) {
                        erp_att_insert_new_shift( $shift['shift_title'], $date['copy_to'], $shift['shift_start_time'], $shift['shift_end_time'], $current_week_data['user_id'] );
                    }
                }
            }

            wp_send_json_success( $current_week_data );
        }
    }

    /**
     * Manages bulk action in shift assign table
     *
     * @since 1.2
     * @return void
     */
    public function manage_bulk_action() {
        if ( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'wp-erp-attendance' ) ) {
            die( 'You are no allowed' );
        }

        $trigger   = isset( $_REQUEST['trigger'] ) && $_REQUEST['trigger'] ? $_REQUEST['trigger'] : '';
        $employees = isset( $_REQUEST['employees'] ) && $_REQUEST['employees'] ? $_REQUEST['employees'] : '';

        if( $trigger ) {
            if( 'delete' == $trigger ) {
                foreach( $employees as $employee ) {
                    if( "true" == $employee['selected'] ) {
                        foreach( $employee['dates'] as $date ) {
                            if( isset( $date['shifts'] ) ) {
                                foreach( $date['shifts'] as $shift ) {
                                    erp_att_delete_shift( $shift['id'] );
                                }
                            }
                        }
                    }
                }

                wp_send_json_success();
            }
        }

    }
}
