<?php
/**
 * Get all attendance
 *
 * @param $args array
 *
 * @return array
 */
function erp_att_get_all_attendance( $args = [] ) {

    $defaults = [
        'number'     => 20,
        'offset'     => 0,
        'orderby'    => 'date',
        'order'      => 'DESC',
    ];

    $args              = wp_parse_args( $args, $defaults );
    $attendance        = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_result = $attendance->select('date')->distinct();

    if ( isset( $args['start'] ) && isset( $args['end'] ) && ! empty( $args['start'] ) && ! empty( $args['end'] ) ) {
        $attendance_result = $attendance_result->where( 'date', '>=', $args['start'] )
                                                ->where( 'date', '<=', $args['end'] );
    }

    $cache_key     = 'attendance-all';
    $items         = wp_cache_get( $cache_key, 'erp-attendance' );

    if ( false === $items ) {

        $results = $attendance_result
                    ->skip( $args['offset'] )
                    ->take( $args['number'] )
                    ->orderBy( $args['orderby'], $args['order'] )
                    ->get()->toArray();
    }

    $items = [];

    foreach ( $results as $single ){
        if( is_future_date( $single['date'] ) ) {
            continue;
        }

        $items[] = [
            'date'    => $single['date'],
            'present' => $attendance->where( 'date', $single['date'] )->where( 'present', '=', 'yes' )->count(),
            'absent'  => $attendance->where( 'date', $single['date'] )->where( 'present', '=', 'no' )->count()
        ];
    }

    $items = erp_array_to_object( $items );

    wp_cache_set( $cache_key, $items, 'erp-attendance' );

    return $items;
}

/**
 * Fetch all attendance from database
 *
 * @return array
 */
function erp_att_get_attendance_count( $args=[] ) {

    $count      = 0;
    $attendance = new \WeDevs\ERP\HRM\Models\Attendance();

    $result = $attendance->select(['date', 'present', 'checkin', 'checkout']);

    if ( isset( $args['start'] ) && isset( $args['end'] ) && ! empty( $args['start'] ) && ! empty( $args['end'] ) ) {
        $result = $result->where('date', '>=', $args['start'])->where('date', '<=', $args['end']);
    }

    $raw_att = $result->select( 'date' )->distinct()->get()->toArray();

    foreach( $raw_att as $att ) {
        if( is_future_date( $att['date'] ) ) {
            continue;
        }

        $count++;
    }

    return $count;
}

/**
 * Get Lists of months for attendance filter
 *
 * @since 1.0
 *
 * @return array
 */
function erp_att_get_filters() {

   $filters = array(
       'today'        => __( 'Today', 'erp-attendance' ),
       'yesterday'    => __( 'Yesterday', 'erp-attendance' ),
       'this_month'   => __( 'This Month', 'erp-attendance' ),
       'last_month'   => __( 'Last Month', 'erp-attendance' ),
       'this_quarter' => __( 'This Quarter', 'erp-attendance' ),
       'last_quarter' => __( 'Last Quarter', 'erp-attendance' ),
       'this_year'    => __( 'This Year', 'erp-attendance' ),
       'last_year'    => __( 'Last Year', 'erp-attendance' ),
       'custom'       => __( 'Custom', 'erp-attendance' )
        );

   return $filters;
}

/**
 * Get start and end date from a specific time
 *
 * @return  array
 */
function erp_att_get_start_end_date( $time = '' ) {

    $duration = [];

    if ( $time ) {

        switch ( $time ) {

            case 'today':

                $start_date = current_time( "Y-m-d" );
                $end_date   = $start_date;
            break;

            case 'yesterday':

                $today      = strtotime( current_time( "Y-m-d" ) );
                $start_date = date( "Y-m-d", strtotime( "-1 days", $today ) );
                $end_date   = $start_date;
            break;

            case 'last_7_days':

                $end_date   = current_time( "Y-m-d" );
                $start_date = date( "Y-m-d", strtotime( "-6 days", strtotime( $end_date ) ) );
            break;

            case 'this_month':

                $start_date = date( "Y-m-d", strtotime( "first day of this month" ) );
                $end_date   = date( "Y-m-d", strtotime( "last day of this month" ) );
            break;

            case 'last_month':

                $start_date = date( "Y-m-d", strtotime( "first day of previous month" ) );
                $end_date   = date( "Y-m-d", strtotime( "last day of previous month" ) );
            break;

            case 'this_quarter':

                $current_month = date( 'm' );
                $current_year  = date( 'Y' );

                if ( $current_month >= 1 && $current_month <= 3 ){

                    $start_date = date( 'Y-m-d', strtotime( '1-January-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '31-March-'.$current_year ) );

                } else  if ( $current_month >= 4 && $current_month <= 6 ){

                    $start_date = date( 'Y-m-d', strtotime( '1-April-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '30-June-'.$current_year ) );

                } else  if ( $current_month >= 7 && $current_month <= 9){

                    $start_date = date( 'Y-m-d', strtotime( '1-July-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '30-September-'.$current_year ) );

                } else  if ( $current_month >= 10 && $current_month <= 12 ){

                    $start_date = date( 'Y-m-d', strtotime( '1-October-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '31-December-'.$current_year ) );
                }
            break;

            case 'last_quarter':

                $current_month = date( 'm' );
                $current_year  = date( 'Y' );

                if ( $current_month >= 1 && $current_month <= 3 ) {

                    $start_date = date( 'Y-m-d', strtotime( '1-October-'.( $current_year-1 ) ) );
                    $end_date   = date( 'Y-m-d', strtotime( '31-December-'.( $current_year-1 ) ) );

                } else if( $current_month >=4 && $current_month <= 6){

                    $start_date = date( 'Y-m-d', strtotime( '1-January-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '31-March-'.$current_year ) );

                } else if( $current_month >= 7 && $current_month <= 9){

                    $start_date = date( 'Y-m-d', strtotime( '1-April-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '30-June-'.$current_year ) );

                } else if( $current_month >= 10 && $current_month <= 12 ){

                    $start_date = date( 'Y-m-d', strtotime( '1-July-'.$current_year ) );
                    $end_date   = date( 'Y-m-d', strtotime( '30-September-'.$current_year ) );
                }
            break;

            case 'last_year':

                $start_date = date( "Y-01-01", strtotime( "-1 year" ) );
                $end_date   = date( "Y-12-31", strtotime( "-1 year" ) );
            break;

            case 'this_year':

                $start_date = date( "Y-01-01" );
                $end_date   = date( "Y-12-31" );
            break;

            default:
            break;
        }
    }

    $duration   = [
        'start' => $start_date,
        'end'   => $end_date
    ];

    return $duration;
}

/**
 * Get attendance for a single date
 *
 * @return  array
 */
function erp_att_get_single_attendance( $args ) {

    global $wpdb;

    $defaults = array(
        'number'     => 20,
        'offset'     => 0,
        'orderby'    => 'date',
        'order'      => 'DESC',
    );

    $args = wp_parse_args( $args, $defaults );

    $attendance = new \WeDevs\ERP\HRM\Models\Attendance();

    $cache_key     = 'attendance-single';
    $items         = wp_cache_get( $cache_key, 'erp-attendance' );

    if ( false === $items ) {
        $results = $attendance
                    ->where( 'date', $args['date'] )
                    ->skip( $args['offset'] )
                    ->take( $args['number'] )
                    ->orderBy( $args['orderby'], $args['order'] )
                    ->get()->toArray();
    }

    $employees    = erp_hr_get_employees( ['number' => -1] );
    $departments  = erp_hr_get_departments_dropdown_raw();

    foreach ( $results as $single ){

        foreach ( $employees as $employee ) {

            if ( $employee->id == $single['user_id'] ) {

                $worktime = 0;
                $items[]  = [
                    'id'              => $single['id'],
                    'user_id'         => $employee->id,
                    'employee_id'     => $employee->employee_id,
                    'employee_name'   => $employee->display_name,
                    'department_name' => $employee->department ? $departments[$employee->department] : '',
                    'present'         => $single['present'],
                    'checkin'         => $single['checkin'],
                    'shift'           => $attendance::find( $single['id'] )->shift_title,
                    'checkout'        => $single['checkout'],
                    'worktime'        => erp_hr_calculate_working_time( $single['checkin'], $single['checkout'] )
                ];
            }
        }

    }

    $items = erp_array_to_object( $items );

    wp_cache_set( $cache_key, $items, 'erp-attendance' );

    return $items;
}

/**
 * Calculate Time diference /  Working Time
 *
 * @since 1.0
 *
 * @return string
 */

function erp_hr_calculate_working_time( $start, $end ) {

    $hours   = 0;
    $minutes = 0;

    if( '00:00:00' === $end || null === $start || null === $end ) {
        return $hours . 'h ' . $minutes . 'm';
    }

    if( !empty( $start ) && !empty( $end ) ){

        $start_time = new DateTime('0-0-0 ' . $start );
        $end_time   = new DateTime('0-0-0 ' . $end );

        if( $end_time->getTimestamp() > $start_time->getTimestamp() ) {
            $interval   = $end_time->diff($start_time);
        } else {
            $end_time = $end_time->modify('+1 day');
            $interval   = $end_time->diff($start_time);
        }

        $hours      = $interval->format('%h');
        $minutes    = $interval->format('%i');
    }

    return $hours . 'h ' . $minutes . 'm';
}

/**
 * Get Attendance Single Page count
 * @return integer
 */
function erp_att_get_single_attendance_count( $args ) {

    $attendance = new \WeDevs\ERP\HRM\Models\Attendance();

    if ( isset( $args['date'] ) && !empty( $args['date'] ) ) {
        $count = $attendance->where('date', '=', $args['date'])->get()->count();
    }

    return $count;
}

/**
 * Returns all the days between two dates
 *
 * @param  date $date_start
 * @param  date $date_end
 * @return object Datepiriod instance
 *
 * @since 1.0
 */
function calculate_date_period( $date_start, $date_end ) {

    $begin         = new \DateTime( $date_start );
    $end           = new \DateTime( $date_end );
    $end           = $end->modify( '+1 day' );
    $interval      = \DateInterval::createFromDateString( '1 day' );

    return new \DatePeriod( $begin, $interval, $end );
}

/**
 * Checks if holiday or leave exists on given date
 *
 * @param  date  $date
 * @param  int  $user_id
 * @return string
 *
 * @since 1.0
 */
function is_valid_working_day( $date, $user_id ) {

    $valid_work_day = erp_hr_get_work_days_between_dates( $date, $date );

    if ( !$valid_work_day['days'][0]['count'] ) {
        return 'holiday';
    }

    $leave_exist = erp_hrm_is_leave_recored_exist_between_date( $date, $date, $user_id );

    if ( $leave_exist ) {
        return 'leave';
    }

    return true;
}

/**
 * Calculate work details like late, early left etc.
 * @param  time $shift_start
 * @param  time $shift_end
 * @param  time $checkin
 * @param  time $checkout
 * @param  array  $grace_times
 * @return array
 */
function calculate_work_details_by_date( $shift_start, $shift_end, $checkin, $checkout, $grace_times = [] ) {

    $allowed_time = [
        'before_checkin'  => strtotime( '-'. $grace_times['before_checkin'] . ' minutes', $shift_start ),
        'after_checkin'   => strtotime( '+'. $grace_times['after_checkin'] . ' minutes', $shift_start ),
        'before_checkout' => strtotime( '-'. $grace_times['before_checkout'] . ' minutes', $shift_end ),
        'after_checkout'  => strtotime( '+'. $grace_times['after_checkout'] . ' minutes', $shift_end )
    ];

    $early_entry       = 0;
    $late              = 0;
    $early_left        = 0;
    $extra_time        = 0;

    if ( isset( $checkin ) && isset( $checkout ) && !empty( $checkin ) && !empty( $checkout ) ) {

        $checkin  = strtotime( $checkin );
        $checkout = strtotime( $checkout );

        if ( $checkin < $allowed_time['before_checkin'] ) {
            $early_entry = $shift_start - $checkin;
        }

        if ( $checkin > $allowed_time['after_checkin'] ) {
            $late = $checkin - $shift_start;
        }

        if ( $checkout < $allowed_time['before_checkout'] ) {
            $early_left = $shift_end - $checkout;
        }

        if ( $checkout > $allowed_time['after_checkout'] ) {
            $extra_time = $checkout - $shift_end;
        }
    }

    $data = [
        'early_entry' => $early_entry,
        'late'        => $late,
        'early_left'  => $early_left,
        'extra_time'  => $extra_time
    ];

    return $data;
}

/**
 * Get Department ID by User ID
 *
 * @param int $user_id
 * @return int
 */
function erp_att_get_user_dept( $user_id ) {
    global $wpdb;

    $dept_id = $wpdb->get_var( "SELECT department FROM {$wpdb->prefix}erp_hr_employees WHERE user_id = $user_id" );

    return $dept_id;
}

/**
 * Gets attendance record for an user between dates
 *
 * @param  int $user_id
 * @param  date $date_start
 * @param  date $date_end
 * @return object
 */
function erp_att_get_attendance_record_between_dates( $date_start, $date_end, $user_id = 0 ) {
    global $wpdb;

    $query      = 'SELECT user_id, date, present, checkin, checkout FROM ' . $wpdb->prefix . 'erp_attendance WHERE date >= "' . $date_start . '" AND date <= "' . $date_end . '"';

    if ( $user_id ) {
        $query .= ' AND user_id = ' . $user_id;
    }

    $att_record = $wpdb->get_results( $query, ARRAY_A );

    return $att_record;
}

/**
 * Get Office Time
 *
 * @since 1.0
 *
 * @return array
 */
function erp_att_get_office_time() {
    $office_time      = [
        'starts' => erp_get_option( 'office_starts', '', '10:00' ),
        'ends'   => erp_get_option( 'office_ends', '', '18:00' )
    ];

    return $office_time;
}

/**
 * Get Grace Times
 *
 * @since 1.0
 *
 * @return array
 */
function erp_att_get_grace_times() {
    $grace_times      = [
        'before_checkin'  => intval( erp_get_option( 'grace_before_checkin', '', 15 ) ),
        'after_checkin'   => intval( erp_get_option( 'grace_after_checkin', '', 15 ) ),
        'before_checkout' => intval( erp_get_option( 'grace_before_checkout', '', 15 ) ),
        'after_checkout'  => intval( erp_get_option( 'grace_after_checkout', '', 15 ) )
    ];

    return $grace_times;
}

/**
 * Get query times
 *
 * @since 1.0
 *
 * @return array
 */
function erp_att_get_query_times() {
    return [
        'this_month'   => __( 'This Month', 'erp-attendance' ),
        'last_month'   => __( 'Last Month', 'erp-attendance' ),
        'this_quarter' => __( 'This Quarter', 'erp-attendance' ),
        'last_quarter' => __( 'Last Quarter', 'erp-attendance' ),
        'this_year'    => __( 'This Year', 'erp-attendance' ),
        'last_year'    => __( 'Last Year', 'erp-attendance' ),
        'custom'       => __( 'Custom', 'erp-attendance' ),
    ];
}

/**
 * Converts second to hour minute
 *
 * @return string
 */
function erp_att_second_to_hour_min( $sec, $format="h" ) {

    $init    = $sec;
    $hours   = floor($init / 3600);
    $minutes = floor(($init / 60) % 60);
    // $seconds = $init % 60;
    if ( 'h' == $format ) {
        return $hours . 'h ' . $minutes . 'm';
    }

    if ( ':' == $format ) {
        $time = date('h:i A', strtotime( '0000-00-00' ) + $sec );
        return $time;
    }

    return false;
}

/**
 * Checks if the string is valid date time
 *
 * @param $date
 * @param string $format
 *
 * @return bool
 */
function erp_att_validate_date_time($date, $format = 'Y-m-d H:i:s') {

    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/**
 * Crates new Attendance if same entry not exists
 *
 * @param $shift_id
 * @param $checkin
 * @param $checkout
 *
 * @return object Eloquent Attendance model object
 */
function erp_att_insert_attendance( $shift_id, $present, $checkin, $checkout ) {
    $attendance = \WeDevs\ERP\HRM\Models\Attendance::find( absint( $shift_id ) );
    $attendance->present  = $present;
    $attendance->checkin  = $checkin;
    $attendance->checkout = $checkout;
    $attendance->save();

    return $attendance;
}

/**
 * Update single attendance
 *
 * @param $attendance_id
 * @param $user_id
 * @param $shift_id
 * @param $date
 * @param $checkin
 * @param $checkout
 */
function erp_att_update_attendance( $attendance_id, $user_id, $shift_id, $date, $checkin, $checkout ) {
    $attendance = new \WeDevs\ERP\HRM\Models\Attendance();

    $single_attendance = $attendance::find( $attendance_id );

    $single_attendance->user_id  = $user_id;
    $single_attendance->shift_id = $shift_id;
    $single_attendance->date     = $date;
    $single_attendance->checkin  = $checkin;
    $single_attendance->checkout = $checkout;

    $single_attendance->save();
}

/**
 * Delete single attendance record
 *
 * @param $attendance_id
 */
function erp_att_delete_attendance( $attendance_id ) {
    $attendance = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance->destroy( $attendance_id );
}


/**
 * Get a single attendace details by attendance id
 *
 * @param $attendance_id int
 * $param $length_in
 *
 * @return array
 */
function erp_att_get_attendace_by_id( $attendance_id, $length_in ) {

    $attendance         = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_details = $attendance::find( $attendance_id )->toArray();

    // Calculate shift length
    $shift_details['length'] = erp_att_get_time_lenght( $attendance_details['date'], $attendance_details['start_time'], $attendance_details['end_time'], $length_in );

    // returns id, user_id, title, date, start_time, end_time
    return $shift_details;
}

/**
 * Get all attendance record on a specific date
 *
 * @param $date
 * @param $lenght_in
 *
 * @return array
 */
function erp_att_get_attendance_by_date( $date, $lenght_in = 'second' ) {

    $attendance         = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_details = $attendance->where( 'date', '=', $date )->get()->toArray();
    $total_length       = 0;

    // Calculate shift length
    foreach( $attendance_details as &$s ) {
        $s['length'] = $s['checkin'] && $s['checkout'] ? erp_att_get_time_lenght( $s['date'], $s['checkin'], $s['checkout'], $lenght_in ) : 0;
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'attendance'   => $attendance_details,
        'count'        => count( $attendance_details ),
        'total_lenght' => $total_length
    ];
}

/**
 * Get all attendance record in a date range
 *
 * @param $start_date
 * @param $end_date
 * @param $lenght_in
 *
 * @return array
 */
function erp_att_get_attendance_by_daterange( $start_date, $end_date, $lenght_in = 'second' ) {

    $attendance         = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_details = $attendance->where( 'date', '>=', $start_date )->where( 'date', '<=', $end_date )->get()->toArray();
    $total_length       = 0;

    // Calculate shift length
    foreach( $attendance_details as &$s ) {

        $s['length']  = $s['checkin'] && $s['checkout'] ? erp_att_get_time_lenght( $s['date'], $s['checkin'], $s['checkout'], $lenght_in ) : 0;
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'shifts'       => $attendance_details,
        'count'        => count( $attendance_details ),
        'total_lenght' => $total_length
    ];
}

/**
 * Get attendance record for an employee on a specific date
 * @param $date
 * @param $user_id
 * @param $lenght_in
 *
 * @return array
 */
function erp_att_get_attendance_by_date_by_employee( $date, $user_id, $lenght_in ) {

    $attendance         = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_details = $attendance->where( 'date', '=', $date )->where( 'user_id', '=', $user_id )->get()->toArray();
    $total_length       = 0;

    // Calculate shift length
    foreach( $attendance_details as &$s ) {
        $s['length'] = $s['checkin'] && $s['checkout'] ? erp_att_get_time_lenght( $s['date'], $s['checkin'], $s['checkout'], $lenght_in ) : 0;
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'attendance'   => $attendance_details,
        'count'        => count( $attendance_details ),
        'total_lenght' => $total_length
    ];
}

/**
 * Get attendance record for a date range for an employee
 *
 * @param $user_id
 * @param $start_date
 * @param $end_date
 * @param $lenght_in
 *
 * @return array
 */
function erp_att_get_attendance_by_daterange_by_employee( $user_id, $start_date, $end_date, $lenght_in ) {

    $attendance         = new \WeDevs\ERP\HRM\Models\Attendance();
    $attendance_details = $attendance->where( 'date', '>=', $start_date )->where( 'date', '<=', $end_date )->where( 'user_id', '=', $user_id )->get()->toArray();
    $total_length       = 0;

    // Calculate shift length
    foreach( $attendance_details as &$s ) {

        $s['length']  = ('yes' == $s['present'] && null != $s['checkin'] && null != $s['checkout'] ) ? erp_att_get_time_lenght( $s['date'], $s['checkin'], $s['checkout'], $lenght_in ) : 0;
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'attendance'   => $attendance_details,
        'count'        => count( $attendance_details ),
        'total_length' => $total_length
    ];
}

/**
 * Get attendance data within a date range
 *
 * @since 1.1.0
 *
 * @param string  $start
 * @param string  $end
 * @param integer $user_id
 * @param boolean $filtered
 *
 * @return array|object in case of not filtered Illuminate Collection object will be returned
 */
function erp_att_get_attendance_data( $start, $end, $user_id = 0, $filtered = false ) {
    $query = \WeDevs\ERP\HRM\Models\Attendance::where( 'date', '>=', $start )->where( 'date', '<=', $end );

    if ( ! empty( $user_id ) ) {
        $query->where( 'user_id', $user_id );
    }

    $query = $query->get();

    if ( ! $filtered ) {
        return $query;
    }

    if ( ! $query->count() ) {
        return [];
    }

    $absents         = collect();
    $late_checkins   = collect();
    $proper_checkins = collect();

    $query->each( function ( $entry ) use ( &$proper_checkins, &$late_checkins, &$absents ) {
        if ( ! empty( $entry->shift_start_time ) && ! empty( $entry->checkin ) ) {
            $shift_start = explode( ':', $entry->shift_start_time );
            $shift_start = \Carbon\Carbon::createFromTime( $shift_start[0], $shift_start[1], $shift_start[2] );

            $checkin = explode( ':', $entry->checkin );
            $checkin = \Carbon\Carbon::createFromTime( $checkin[0], $checkin[1], $checkin[2] );

            if ( $shift_start->gte( $checkin ) ) {
                $proper_checkins->push( $entry );
            } else {
                $late_checkins->push( $entry );
            }
        }

        if ( 'no' === $entry->present ) {
            $absents->push( $entry );
        }
    } );

    return [
        'absents'           => $absents,
        'late_checkins'     => $late_checkins,
        'proper_checkins'   => $proper_checkins,
    ];
}

/**
 * Save self attendance
 *
 * @since 1.1.0
 *
 * @param string $type
 *
 * @return object Eloquent Attendance model or WP_Error object
 */
function save_self_attendance( $type ) {
    $att_model          = \WeDevs\ERP\HRM\Models\Attendance::query();
    $user_id            = get_current_user_id();
    $today              = current_time('Y-m-d');
    $current_time       = current_time('H:i:s');

    if( is_shift_enabled() && isset( $_POST['shift_id'] ) ) {
        $shift_id = absint( $_POST['shift_id'] );
        $existing_entry = $att_model->find( $shift_id );

        if ( $existing_entry && $existing_entry->user_id == $user_id ) {
            if ( 'checkout' === $type && $existing_entry->checkin && ! $existing_entry->checkout ) {
                $existing_entry->checkout = $current_time;
                $existing_entry->save();

            } else if ( 'checkin' === $type && ! $existing_entry->checkin ) {
                $existing_entry->checkin = $current_time;
                $existing_entry->present = 'yes';
                $existing_entry->save();
            }

            return $existing_entry;
        }

    } else if ( 'checkin' === $type ){
        $office_time = erp_att_get_office_time();
        $shift_id    = erp_att_insert_new_shift( '', $today, $office_time['starts'], $office_time['ends'], $user_id );

        if( $shift_id ) {
            $saved_attendance = erp_att_insert_attendance( $shift_id, 'yes', $current_time, null );

            return $saved_attendance;

        } else {
            return new \WP_Error( 'erp_att_checkin_error', __( 'Error occured while checkin, please try again.', 'erp-attendance' ) );
        }

    } else if ( 'checkout' === $type ) {
        $existing_entry = $att_model->where( 'user_id', $user_id )->where( 'date',  $today )->first();

        if( $existing_entry ) {
            $existing_entry->update( ['checkout' => $current_time ] );

            return $existing_entry;

        } else {
            return new \WP_Error( 'erp_att_no_checkin_entry', __( 'You do not have any entry for checkin. Please checkin first.', 'erp-attendance' ) );
        }
    }

    return new \WP_Error( 'erp_att_self_attendance_error', __( 'Could not save your attendance, please try again', 'erp-attendance' ) );
}