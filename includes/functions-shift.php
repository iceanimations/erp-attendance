<?php
/**
 * New Shift
 *
 * @since 1.0
 *
 *@return array
 */
function get_empty_shift() {
    $shift = [
        'shift_name' => '',
        'start' => '',
        'end' => '',
        'date_from' => '',
        'date_to' => '',
        'apply_to' => '',
        'day_sun' => '',
        'day_mon' => '',
        'day_tue' => '',
        'day_wed' => '',
        'day_thu' => '',
        'day_fri' => '',
        'day_sat' => '',

    ];

    return $shift;
}

/**
 * Create new shift
 *
 * @param $title
 * @param $date
 * @param $start_time
 * @param $end_time
 * @param $user_id
 *
 * @return mixed
 */
function erp_att_insert_new_shift( $title, $date, $start_time, $end_time, $user_id ) {
    $shift = \WeDevs\ERP\HRM\Models\Attendance::firstOrCreate( [
        'shift_title'      => $title,
        'date'             => $date,
        'shift_start_time' => $start_time,
        'shift_end_time'   => $end_time,
        'user_id'          => $user_id
    ] );

    return $shift->id;
}

function erp_att_update_or_insert_new_shift( $shift_id, $title, $date, $start_time, $end_time, $user_id ) {
    $shift = new \WeDevs\ERP\HRM\Models\Shift();

    $shift->updateOrCreate( [
        'id' => $shift_id
    ],
        [
            'title'      => $title,
            'date'       => $date,
            'start_time' => $start_time,
            'end_time'   => $end_time,
            'user_id'    => $user_id
        ] );
}

/**
 * Deletes single shift
 *
 * @param $shift_id
 */
function erp_att_delete_shift( $shift_id ) {
    $shift = new \WeDevs\ERP\HRM\Models\Attendance();
    $shift->destroy( $shift_id );
}

/**
 * Get a single shift details by shift id
 *
 * @param $shift_id int
 * @param $length_in
 *
 * @return array
 */
function erp_att_get_shift_by_id( $shift_id, $length_in = 'second' ) {

    $shift         = new \WeDevs\ERP\HRM\Models\Attendance();
    $shift_details = $shift::find( $shift_id )->toArray();

    // Calculate shift length
    $shift_details['length'] = erp_att_get_time_lenght( $shift_details['date'], $shift_details['shift_start_time'], $shift_details['shift_end_time'], $length_in );

    // returns id, user_id, title, date, start_time, end_time
    return $shift_details;
}

/**
 * Get shift length
 *
 * @param $length_in Hours Minutes Seconds
 *
 * @return int
 */
function erp_att_get_time_lenght( $date, $start_time, $end_time, $length_in = 'second' ) {

    $start_time    = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date . $start_time );
    $end_time      = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date . $end_time );

    if( 'second' == $length_in ) {
        return $start_time->diffInSeconds( $end_time );
    }

    if( 'minute' == $length_in ) {
        return $start_time->diffInMinutes( $end_time );
    }

    if( 'hour' == $length_in ) {
        return $start_time->diffInHours( $end_time );
    }

    if( 'day' == $length_in ) {
        return $start_time->diffInDays( $end_time );
    }
}

/**
 * Get a single shift details by date
 *
 * @param $shift_id int
 *
 * @return array
 */
function erp_att_get_shift_by_date( $date, $lenght_in = 'second' ) {

    $shift         = new \WeDevs\ERP\HRM\Models\Attendance();
    $shift_details = $shift->where( 'date', '=', $date )->get()->toArray();
    $total_length  = 0;

    // Calculate shift length
    foreach( $shift_details as &$s ) {
        $s['length'] = erp_att_get_time_lenght( $s['date'], $s['shift_start_time'], $s['shift_end_time'], $lenght_in );
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'shifts'       => $shift_details,
        'count'        => count( $shift_details ),
        'total_lenght' => $total_length
    ];
}


/**
 * Get a single shift details by date
 *
 * @param $start_date, $end_date, $length_in
 *
 * @return array
 */
function erp_att_get_shift_by_daterange( $start_date, $end_date, $lenght_in = 'second' ) {

    $shift         = new \WeDevs\ERP\HRM\Models\Attendance();
    $shift_details = $shift->where( 'date', '>=', $start_date )->where( 'date', '<=', $end_date )->get()->toArray();
    $total_length  = 0;

    // Calculate shift length
    foreach( $shift_details as &$s ) {

        $s['length']  = erp_att_get_time_lenght( $s['date'], $s['shift_start_time'], $s['shift_end_time'], $lenght_in );
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'shifts'       => $shift_details,
        'count'        => count( $shift_details ),
        'total_lenght' => $total_length
    ];
}


/**
 * Get shift details by date by an employee
 *
 * @param $shift_id int
 *
 * @return array
 */
function erp_att_get_shift_by_date_by_employee( $date, $user_id, $lenght_in = 'second' ) {

    $shift         = new \WeDevs\ERP\HRM\Models\Attendance();
    $shift_details = $shift->where( 'date', '=', $date )->where( 'user_id', '=', $user_id )->get()->toArray();
    $total_length  = 0;

    // Calculate shift length
    foreach( $shift_details as &$s ) {
        $s['length'] = erp_att_get_time_lenght( $s['date'], $s['shift_start_time'], $s['shift_end_time'], $lenght_in );
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'shifts'       => $shift_details,
        'count'        => count( $shift_details ),
        'total_lenght' => $total_length
    ];
}


/**
 * Get a single shift details by date
 *
 * @param $start_date, $end_date, $length_in
 *
 * @return array
 */
function erp_att_get_shift_by_daterange_by_employee( $start_date, $end_date, $user_id, $lenght_in = 'second' ) {

    $shift         = new \WeDevs\ERP\HRM\Models\Shift();
    $shift_details = $shift->where( 'date', '>=', $start_date )->where( 'date', '<=', $end_date )->where( 'user_id', '=', $user_id )->get()->toArray();
    $total_length  = 0;

    // Calculate shift length
    foreach( $shift_details as &$s ) {

        $s['length']  = erp_att_get_time_lenght( $s['date'], $s['shift_start_time'], $s['shift_end_time'], $lenght_in );
        $total_length += $s['length'];
    }

    // returns id, user_id, title, date, start_time, end_time, count and total_length
    return [
        'shifts'       => $shift_details,
        'count'        => count( $shift_details ),
        'total_lenght' => $total_length
    ];
}

/**
 * Checks if shift is enabled
 * @return bool
 */
function is_shift_enabled() {
    return 'yes' == erp_get_option( 'enable_shift' ) ? true : false;
}

/**
 * Check if future date
 * @param $date
 *
 * @return bool
 */
function is_future_date( $date ) {
    return strtotime( $date ) > strtotime( current_time( 'Y-m-d' ) ) ? true : false;
}

