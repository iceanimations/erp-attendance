<?php
namespace WeDevs\ERP\HRM\Attendance;

/**
 * Installation related functions and actions
 *
 * @since 1.1.0
 */
class Install {

    /**
     * Class constructor
     *
     * @since 1.1.0
     */
    public function __construct() {
        if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || ( defined( 'DOING_CRON' ) && DOING_CRON ) ) {
            return;
        }

        register_activation_hook( WPERP_ATTEND_FILE, array( $this, 'activate' ) );
    }

    /**
     * Plugin activation hook
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function activate() {
        $current_version = get_option( 'erp-attendance-version', null );

        // create table
        $this->create_table();

        // does it needs any update?
        if ( ! class_exists( '\WeDevs\ERP\HRM\Attendance\Updates' ) ) {
            include_once WPERP_ATTEND_INCLUDES . '/class-updates.php';
        }

        $updater = new \WeDevs\ERP\HRM\Attendance\Updates();
        $updater->perform_updates();

        // update to latest version
        update_option( 'erp-attendance-version', WPERP_ATTEND_VERSION );
    }

    /**
     * Create plugin tables
     *
     * @since 1.1.0
     *
     * @return void
     */
    private function create_table() {
        global $wpdb;

        $collate = '';

        if ( $wpdb->has_cap( 'collation' ) ) {
            if ( ! empty($wpdb->charset ) ) {
                $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
            }

            if ( ! empty($wpdb->collate ) ) {
                $collate .= " COLLATE $wpdb->collate";
            }
        }

        $table_schema = [
            "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}erp_attendance (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `user_id` bigint(20) NOT NULL,
              `date` date NOT NULL,
              `shift_title` varchar(255) DEFAULT NULL,
              `shift_start_time` time NOT NULL,
              `shift_end_time` time NOT NULL,
              `present` enum('no','yes') NOT NULL,
              `checkin` time DEFAULT NULL,
              `checkout` time DEFAULT NULL,
              KEY `id` (`id`)
            ) $collate;"
        ];

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        foreach ( $table_schema as $table ) {

            dbDelta( $table );
        }
    }

}

new Install();
