<?php
namespace WeDevs\ERP\HRM\Models;

use WeDevs\ERP\Framework\Model;

/**
 * Attendance Model
 */
class Attendance extends Model {
	protected $table    = 'erp_attendance';
	protected $fillable = [ 'date', 'user_id', 'shift_title', 'shift_start_time', 'shift_end_time', 'checkin', 'checkout' ];
	public $timestamps  = false;

	public function shift() {
		return $this->belongsTo( 'WeDevs\ERP\HRM\Models\Shift', 'shift_id' );
	}
}
