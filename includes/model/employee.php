<?php
namespace WeDevs\ERP\HRM\Models;

use WeDevs\ERP\Framework\Model;

/**
 * Attendance Model
 */
class EmployeeExtended extends Model {

    protected $table    = 'erp_hr_employees';
    protected $fillable = [ 'user_id', 'employee_id', 'status', 'designation', 'department' ];
    public $timestamps  = false;

    public function attendance() {
        return $this->hasMany( 'WeDevs\ERP\HRM\Models\Attendance', 'user_id', 'user_id' );
    }
}