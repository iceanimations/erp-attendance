<?php
namespace WeDevs\ERP\HRM\Attendance;
/**
 * Handle the form submissions
 *
 * Although our most of the forms uses ajax and popup, some
 * are needed to submit via regular form submits. This class
 * Handles those form submission in this module
 */
class Attendance_Form_Handler {

    public function __construct() {
        add_action( 'load-toplevel_page_erp-hr-attendance', array( $this, 'handle_attendance_main' ) );
        add_action( 'load-hr-management_page_erp-hr-employee', array( $this, 'employee_att_report' ) );
        add_action( 'load-hr-management_page_erp-hr-reporting', array( $this, 'hr_reporting_att_report' ) );
        add_action( 'load-toplevel_page_erp-hr-attendance', array( $this, 'sinlge_employee_record_page' ) );
    }

    /**
     * Check is current page actions
     *
     * @since 0.1
     *
     * @param  integer $page_id
     * @param  integer $bulk_action
     *
     * @return boolean
     */
    public function verify_current_page_screen( $page_id, $bulk_action ) {

        if ( ! isset( $_REQUEST['_wpnonce'] ) || ! isset( $_GET['page'] ) ) {
            return false;
        }

        if ( $_GET['page'] != $page_id ) {
            return false;
        }

        if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], $bulk_action ) ) {
            return false;
        }

        return true;
    }

    /**
     * Form handler for employee single page report
     *
     * @since 1.0
     */
    public function employee_att_report() {

        if ( ! $this->verify_current_page_screen( 'erp-hr-employee', 'epr-attendance-filter' ) ) {
            return;
        }

        $query = isset( $_REQUEST['query_time'] ) ? $_REQUEST['query_time'] : '';

        if ( isset( $_REQUEST['filter_attendance'] ) ) {

            $redirect = remove_query_arg( ['_wp_http_referer', '_wpnonce'], wp_unslash( $_SERVER['REQUEST_URI'] ) );
            $redirect = remove_query_arg( ['filter_attendance'], $redirect );

            wp_redirect( $redirect );
        }
    }

    /**
     * Attendance report on HR reporting form handler
     * @return void
     *
     * @since 1.0
     */
    public function hr_reporting_att_report() {

        if ( ! $this->verify_current_page_screen( 'erp-hr-reporting', 'epr-att-hr-reporting-filter' ) ) {
            return;
        }

        $department_id = isset( $_REQUEST['department'] ) ? $_REQUEST['department'] : '';
        $query_time    = isset( $_REQUEST['query_time'] ) ? $_REQUEST['query_time'] : '';

        if ( isset( $_REQUEST['filter_att_hr_reporting'] ) ) {

            $redirect = remove_query_arg( ['_wp_http_referer', '_wpnonce'], wp_unslash( $_SERVER['REQUEST_URI'] ) );
            $redirect = remove_query_arg( ['filter_att_hr_reporting'], $redirect );

            wp_redirect( $redirect );
        }

    }

    /**
     * Handles for submission from Single Employee Attendance Record Page
     *
     * @since 1.1
     * @return bool
     */
    public function sinlge_employee_record_page() {

        if ( ! $this->verify_current_page_screen( 'erp-hr-attendance', 'bulk-attendances_singles' ) ) {
            return;
        }

        $record_table = new \WeDevs\ERP\HRM\Attendance\Attendance_Single();
        $action       = $record_table->current_action();

        if ( $action ) {

            $redirect = remove_query_arg( ['_wp_http_referer', '_wpnonce'], wp_unslash( $_SERVER['REQUEST_URI'] ) );

            switch ( $action ) {

                case 'attendance_delete':
                    if ( isset( $_GET['record_id'] ) && !empty( $_GET['record_id'] ) ) {
                        foreach ( $_GET['record_id'] as $id ) {
                            erp_att_delete_attendance( intval( $id ) );
                        }
                    }

                    $redirect = remove_query_arg( ['record_id'], $redirect );

                    wp_redirect( $redirect );

                    break;

                default:
                    break;
            }
        }

    }

    /**
     * Handle form submission from attendance main table
     *
     * @since 1.2
     *
     * @return void
     */
    public function handle_attendance_main() {
        if ( ! $this->verify_current_page_screen( 'erp-hr-attendance', 'bulk-attendances' ) ) {
            return;
        }

        $attendance_table = new \WeDevs\ERP\HRM\Attendance\Attendance();
        $action           = $attendance_table->current_action();

        if ( $action ) {
            $redirect   = remove_query_arg( ['_wp_http_referer', '_wpnonce'], wp_unslash( $_SERVER['REQUEST_URI'] ) );
            $attendance = new \WeDevs\ERP\HRM\Models\Attendance();

            switch( $action ) {
                case 'delete':
                    if ( isset( $_REQUEST['attendance_date'] ) && !empty( $_REQUEST['attendance_date'] ) ) {
                        foreach ( $_REQUEST['attendance_date'] as $date ) {
                            $attendance->where( 'date', $date )->delete();
                        }
                    }
                    $redirect = remove_query_arg(['attendance_date'], $redirect);
                    wp_redirect( $redirect );
                    exit;

                default:
                    $redirect = remove_query_arg(['filter_attendance'], $redirect);
                    wp_redirect( $redirect );
                    exit;
            }
        }
    }
}

new Attendance_Form_Handler();