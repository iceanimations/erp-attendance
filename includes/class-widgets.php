<?php
namespace WeDevs\ERP\HRM\Attendance;

/**
 * Widgets provided by this plugin
 *
 * @since 1.1.0
 */
class Widgets {
    private $is_scripts_enqueued = false;

    /**
     * Class constructor
     *
     * @since 1.1.0
     *
     * @return void
     */
    function __construct() {
        // Adds an widget in HR dashboard page
        add_action( 'erp_hr_dashboard_widgets_right', [ $this, 'widget_attendance_status' ] );

        // Add self attendance option to employee dashboard
        if ( 'yes' === erp_get_option( 'enable_self_att' ) ) {
            add_action( 'erp_hr_dashboard_widgets_right', [ $this, 'widget_self_checking_checkout' ] );
        }
    }

    /**
     * Register attendance status widget
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function widget_attendance_status() {
        erp_admin_dash_metabox( __( '<i class="fa fa-pie-chart"></i> Attendance Status', 'erp-attendance' ), [ $this, 'widget_attendance_status_view' ] );
    }

    /**
     * Register self checking/checkout widget
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function widget_self_checking_checkout() {
        erp_admin_dash_metabox( __( '<i class="fa fa-clock-o"></i> Attendance Self Service', 'erp-attendance' ), [ $this, 'widget_self_checking_checkout_view' ] );
    }

    /**
     * Include attendance status view
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function widget_attendance_status_view() {
        $this->enqueue_scripts();
        include WPERP_ATTEND_VIEWS . '/widget-attendance-status.php';
    }

    /**
     * Include self checking/checkout view
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function widget_self_checking_checkout_view() {
        $this->enqueue_scripts();
        include WPERP_ATTEND_VIEWS . '/widget-employee-self-service.php';
    }

    /**
     * Enqueue required scripts for widgets
     *
     * @since 1.1.0
     *
     * @return void
     */
    public function enqueue_scripts() {
        if ( $this->is_scripts_enqueued ) {
            return;
        }

        wp_enqueue_style(
            'erp-attendance-widgets',
            WPERP_ATTEND_ASSETS . '/css/erp-attendance-widgets.css',
            [],
            WPERP_ATTEND_VERSION
        );

        wp_enqueue_script(
            'erp-attendance-widgets',
            WPERP_ATTEND_ASSETS . '/js/erp-attendance-widgets.js',
            [ 'jquery', 'erp-vuejs', 'erp-flotchart', 'erp-flotchart-pie' ],
            WPERP_ATTEND_VERSION,
            true
        );

        // localized data
        $erp_attendance_widgets = [
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'nonce'         => wp_create_nonce( 'wp-erp-attendance' ),
            'scriptDebug'   => defined( 'SCRIPT_DEBUG' ) ? SCRIPT_DEBUG : false,
            'i18n'          => [
                'filterBy'          => __( 'Filter By', 'erp-attendance' ),
                'today'             => __( 'Today', 'erp-attendance' ),
                'yesterday'         => __( 'Yesterday', 'erp-attendance' ),
                'thisMonth'         => __( 'This Month', 'erp-attendance' ),
                'lastMonth'         => __( 'Last Month', 'erp-attendance' ),
                'thisQuarter'       => __( 'This Quarter', 'erp-attendance' ),
                'thisYear'          => __( 'This Year', 'erp-attendance' ),
                'notEnoughData'     => __( 'Not enough data', 'erp-attendance' ),
                'loadingData'       => __( 'Loading data', 'erp-attendance' ),
                'noShiftAssigned'   => __( 'No shift is assigned for current time', 'erp-attendance' ),
                'checkin'           => __( 'Checkin', 'erp-attendance' ),
                'checkout'          => __( 'Checkout', 'erp-attendance' ),
                'thanksForCheckin'  => __( 'Your checkin entry is saved. Thank you for checkin.', 'erp-attendance' ),
                'thanksForCheckout' => __( 'Your checkout entry is saved. Thank you.', 'erp-attendance' ),
                'selectShiftFirst'  => __( 'You must select a shift first!', 'erp-attendance' ),
            ]
        ];

        wp_localize_script( 'erp-attendance-widgets', 'erpAttendanceWidgets', $erp_attendance_widgets );

        $this->is_scripts_enqueued = true;
    }

}

