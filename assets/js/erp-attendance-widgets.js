;(function($) {
    'use strict';

    Vue.config.debug = !!(erpAttendanceWidgets.scriptDebug);

    $('.erp-attendance-status-widget').each(function () {
        var widget = this;

        new Vue({
            el: widget,

            data: {
                i18n: erpAttendanceWidgets.i18n,
                filter: 'today',
                attendance_data: [],
                doingAjax: false
            },

            ready: function () {
                var self = this;

                this.getAttendanceData();

                // this event triggers fron self service widget
                $(window).on('erp-att-refresh-status-widget', function (e) {
                    self.getAttendanceData();
                })
            },

            methods: {
                getAttendanceData: function () {
                    var self = this;

                    var data = {
                        query: self.filter || 'today',
                        action: 'erp_att_get_attendance_data',
                        _wpnonce: erpAttendanceWidgets.nonce
                    };

                    self.doingAjax = true;

                    $.get(erpAttendanceWidgets.ajaxurl, data, function(response) {
                        self.doingAjax = false;

                        self.attendance_data = response.data;

                        if (self.attendance_data.length) {
                            setTimeout(function () {
                                self.drawChart();
                            }, 400);
                        }

                    } );
                },

                drawChart: function () {
                    $.plot('#erp-attendance-status-chart-' + this._uid, this.attendance_data, {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,
                                innerRadius: 0.4,
                                label: {
                                    show: true,
                                    radius: 2 / 3,
                                    formatter: function (label, series) {
                                        return '<div class="erp-flot-pie-label">' + series.data[0][1] + '</div>';
                                    },
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true
                            }
                        }
                    } );
                }
            },

            watch: {
                filter: function () {
                    this.getAttendanceData();
                }
            }
        });
    });

    $('.erp-att-self-service-widget').each(function () {
        var widget = this;

        new Vue({
            el: widget,

            data: {
                i18n: erpAttendanceWidgets.i18n,
                is_shift_enabled: false,
                attendance: {
                    id: 0,
                    title: '',
                    checkin: '',
                    checkout: '',
                },
                isReady: false,
                doingAjax: false,
                digital_clock: '',
                server_time: '',
                shift_time: '',
            },

            ready: function() {
                var self = this,
                    data = {
                    action: 'erp_att_get_employee_attendance_data',
                    _wpnonce: erpAttendanceWidgets.nonce
                };

                this.doingAjax = true;

                $.get(erpAttendanceWidgets.ajaxurl, data).done(function(response){
                    self.$set('is_shift_enabled', response.data.is_shift_enabled);

                    if (response.data.attendance.id) {
                        self.$set('attendance', response.data.attendance);
                    } else {
                        self.$set('attendance', {
                            id: 0,
                            title: '',
                            checkin: '',
                            checkout: '',
                        });
                    }

                    self.$set('server_time', response.data.server_time);
                    self.$set('shift_time', response.data.shift_time);
                    self.$set('doingAjax', false);
                    self.$set('isReady', true);
                });

                this.updateClockPerSecond();
            },

            computed: {
                noShiftAssigned: function () {
                    return !(this.is_shift_enabled && this.attendance.id);
                },

                showButtons: function () {
                    if (!this.isReady) {
                        return false;

                    } else if (!this.is_shift_enabled) {
                        return true;

                    } else if (!this.noShiftAssigned) {
                        return true;
                    }

                    return false;
                },

                isCheckinBtnDisabled: function () {
                    if (!this.is_shift_enabled) {
                        return (this.attendance.checkin || this.doingAjax);
                    } else {
                        return (!parseInt(this.attendance.id) || this.attendance.checkin || this.doingAjax);
                    }

                    return false;
                }
            },

            methods: {
                saveAttendance: function (type) {
                    var self = this;

                    if (this.is_shift_enabled && !parseInt(this.attendance.id)) {
                        alert(this.i18n.selectShiftFirst);
                        return false;
                    }

                    this.doingAjax = true;

                    var data = {
                        _wpnonce : erpAttendanceWidgets.nonce,
                        action: 'erp_att_save_self_attendance',
                        shift_id: this.attendance.id,
                        type: type || 'checkin',
                    };

                    $.post(erpAttendanceWidgets.ajaxurl, data).done(function(response){
                        if (!response.success) {
                            alert(response.data);
                            return;
                        }

                        self.$set('attendance', {
                            id: response.data.id,
                            title: response.data.shift_title,
                            checkin: response.data.checkin,
                            checkout: response.data.checkout,
                        });

                        $(window).trigger('erp-att-refresh-status-widget');

                        var msg = 'checkin' === type ? self.i18n.thanksForCheckin : self.i18n.thanksForCheckout;

                        alert(msg);

                    }).always(function () {
                        self.doingAjax = false;
                    });
                },

                updateClockPerSecond: function () {
                    var self           = this,
                        currentTime    = new Date(),
                        currentHours   = currentTime.getHours(),
                        currentMinutes = currentTime.getMinutes(),
                        currentSeconds = currentTime.getSeconds();

                    currentMinutes = ( currentMinutes < 10 ? '0' : '' ) + currentMinutes;
                    currentSeconds = ( currentSeconds < 10 ? '0' : '' ) + currentSeconds;

                    var timeOfDay = ( currentHours < 12 ) ? 'AM' : 'PM';

                    currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
                    currentHours = ( currentHours == 0 ) ? 12 : currentHours;


                    this.digital_clock = currentHours + ':' + currentMinutes + ':' + currentSeconds + ' ' + timeOfDay;

                    setTimeout(function () {
                        self.updateClockPerSecond();
                    }, 1000);
                }
            },
        });
    });


})(jQuery);