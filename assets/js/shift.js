(function($){
    'use strict';

    if (!$('#erp-att-shifts').length) {
        return;
    }

    Vue.config.devtools = true;

    Vue.directive('erp-sortable', {
        params: [
            'appendTo', 'axis', 'cancel', 'connectWith', 'containment', 'cursor', 'cursorAt',
            'dropOnEmpty', 'forcePlaceholderSize', 'forceHelperSize', 'grid', 'handle',
            'helper', 'items', 'opacity', 'placeholder', 'revert', 'scroll', 'scrollSensitivity',
            'scrollSpeed', 'scope', 'tolerance', 'zIndex',

            // callbacks
            'activate', 'beforeStop', 'change', 'deactivate', 'out', 'over', 'receive', 'remove', 'sort',
            'start', 'stop', 'update'
        ],

        bind: function() {
            var settings = $.extend({
                appendTo: 'parent',
                axis: false,
                cancel: '.ignore-sortable',
                connectWith: false,
                containment: false,
                cursor: 'auto',
                cursorAt: false,
                dropOnEmpty: true,
                forcePlaceholderSize: false,
                forceHelperSize: false,
                grid: false,
                handle: false,
                helper: 'original',
                items: '> *',
                opacity: false,
                placeholder: false,
                revert: false,
                scroll: true,
                scrollSensitivity: 20,
                scrollSpeed: 20,
                scope: 'default',
                tolerance: 'intersect',
                zIndex: 1000,
            }, this.params);

            // Callbacks
            settings.activate = this.params.activate ? this.vm[this.params.activate] : null;
            settings.beforeStop = this.params.beforeStop ? this.vm[this.params.beforeStop] : null;
            settings.change = this.params.change ? this.vm[this.params.change] : null;
            settings.deactivate = this.params.deactivate ? this.vm[this.params.deactivate] : null;
            settings.out = this.params.out ? this.vm[this.params.out] : null;
            settings.over = this.params.over ? this.vm[this.params.over] : null;
            settings.receive = this.params.receive ? this.vm[this.params.receive] : null;
            settings.remove = this.params.remove ? this.vm[this.params.remove] : null;
            settings.sort = this.params.sort ? this.vm[this.params.sort] : null;
            settings.start = this.params.start ? this.vm[this.params.start] : null;
            settings.stop = this.params.stop ? this.vm[this.params.stop] : null;
            settings.update = this.params.update ? this.vm[this.params.update] : null;

            $(this.el).sortable(settings);
        }
    });

    var wperpShift = new Vue({

        el: '#erp-att-shifts',

        data: {
            employees:[],
            searchQuery: '',
            selectAll: false,
            bulkAction: "-1"
        },

        computed: {
            isReady: function () {
                return this.employees.length;
            }
        },

        methods: {
            toggleAllSelect: function() {
                if(!this.selectAll) {
                    for(i in this.employees) {
                        this.employees[i].selected = true;
                    }
                } else {
                    for(i in this.employees) {
                        this.employees[i].selected = false;
                    }
                }
            },

            triggerBulkAction: function() {
                var data = {
                    trigger: this.bulkAction,
                    employees: this.employees,
                    action: 'erp_att_bulk_action',
                    nonce: wpErpAttendance.nonce
                };

                $.post(ajaxurl, data).done(function(data) {
                    location.reload();
                });
            },

            deleteShift: function(shift, dateIndex, employeeIndex) {

                var confimed = confirm(wpErpAttendance.shift_delete_warning);
                if(confimed) {

                    var self = this;

                    var data = {
                        shift_id: shift.id,
                        action: 'erp_att_delete_single_shift',
                        nonce: wpErpAttendance.nonce
                    };

                    $.post(ajaxurl, data).done(function(data) {
                        self.employees[employeeIndex].dates[dateIndex].shifts.$remove(shift);
                    });

                }
            },
            getPreviousWeek: function(date) {
                var data = {
                    date: date,
                    week: 'previous',
                    action: 'erp_get_att_weekly_shift_data',
                    nonce: wpErpAttendance.nonce
                };

                $.post(ajaxurl, data).done(function(data) {
                    wperpShift.employees = data.data;
                });
            },
            getNextWeek: function(date) {
                var data = {
                    date: date,
                    week: 'next',
                    action: 'erp_get_att_weekly_shift_data',
                    nonce: wpErpAttendance.nonce
                };

                $.post(ajaxurl, data).done(function(data) {
                    wperpShift.employees = data.data;
                });
            },
            copyShifts: function(element) {
                var data = {
                    date: element.dates[0].date,
                    current_week_data: element,
                    action: 'erp_att_copy_shifts_to_next_week',
                    nonce: wpErpAttendance.nonce
                };

                $.post(ajaxurl, data).done(function(data) {
                    $('#notice-shift-copied-success').show().delay(1000).fadeOut();
                });
            }
        },

        ready: function() {
            var data = {
                week: 'current',
                action: 'erp_get_att_weekly_shift_data',
                nonce: wpErpAttendance.nonce
            };

            $.post(ajaxurl, data).done(function(data) {
                wperpShift.employees = data.data;
            });
        }
    });

    var vm = new Vue({
        el: "#shift-list",
        data: {
            shifts:[],
        },
        methods:{
            addNewShift: function() {
                this.shifts.push({
                    shift_title: '',
                    shift_start_time: '',
                    shift_end_time: '',
                    edit: true,
                    copy: {
                        shift_title: '',
                        shift_start_time: '',
                        shift_end_time: ''
                    }
                });
            },
            deleteShift: function(shift) {
                this.shifts.$remove(shift);
            },
            editOpen: function(element) {
                element.edit = true;
                element.copy = $.extend({}, element);
                element.copy.edit = false;
            },
            editDone: function(element) {
                element.edit = false;
                $.extend(element, element.copy);
            },
            editCancel: function(element) {
                element.edit = false;
            }
        },
        ready: function() {
            var self = this,
                data = {
                action: 'erp_att_get_shifts',
                nonce: wpErpAttendance.nonce
            };

            $.post(ajaxurl, data).done(function(data) {

                for ( var i in data.data ) {
                    data.data[i].edit = false;
                }

                self.shifts = data.data || [];
            });
        }
    });

    $('#save-changes').on('click', function() {
        var data = {
            action: 'erp_att_save_weekly_shifts',
            nonce: wpErpAttendance.nonce,
            shift_list: vm.shifts,
            shift_data: wperpShift.employees,
        };

        $('.spinner').addClass('is-active');

        $.post(ajaxurl, data).done(function(data) {
            $('.spinner').removeClass('is-active');
            $('#notice-shift-save-success').show().delay(1000).fadeOut();
            location.reload();
        });
    });
})(jQuery);