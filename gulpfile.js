//modules
var gulp=require('gulp');
var del=require('del');
var zip=require('gulp-zip');
var livereload=require('gulp-livereload');
var fs=require('fs');
var path=require('path');

// determine package version
var pkg = JSON.parse(fs.readFileSync('./package.json').toString());


// folders that are part of the package
var publishFolder = {
    css             : 'assets/css/*.css',
    js              : 'assets/js/*.js',
    models          : 'includes/model/*.php',
    updates         : 'includes/updates/*.php',
    includes        : 'includes/*.php',
    libs            : 'includes/libs/*.php',
    languages       : 'languages/*.pot',
    views           : 'views/*.php',
    jstemplates     : 'views/js-templates/*.php',
    root            : 'erp-food.php',
    changelog       : 'CHANGELOG.md'
};

 //deploy and release folders
var pluginBase =  './web/app/plugins/';
var buildBase =  './build/';
var pluginFolder = pluginBase+pkg.name+'/';
var buildFolder = buildBase+pkg.name+'/';

var devcopy_tasks = [];
var build_tasks = [];

process.stdout.write(pluginFolder);
process.stdout.write("\n");
process.stdout.write(buildFolder);
process.stdout.write("\n");

function create_copy_task_function(key, output_folder){
    return function() {
        var files = publishFolder[key];
        return gulp.src('./'+files).
            pipe(gulp.dest(output_folder+path.dirname(files))).
            pipe(livereload());
    };
}

// creating copy tasks for all pubfolders
for (var key in publishFolder){
    var devcopy_task='devcopy_'+key;
    gulp.task(devcopy_task, create_copy_task_function(key, pluginFolder));
    devcopy_tasks.push(devcopy_task);

    var build_task='build_'+key;
    gulp.task(build_task, create_copy_task_function(key, buildFolder));
    build_tasks.push(build_task);
}

// cleanup plugin directory
gulp.task('clean:devcopy', function (){
    del([pluginFolder]).then(function(files) {
        process.stdout.write("deleted " + files.length.toString() + " plugin objects\n");
    });
});

// cleanup build directory
gulp.task('clean:build', function (){
    del([buildBase]).then(function(files) {
        process.stdout.write("deleted " + files.length.toString() + " build objects\n");
    });
});

// watch all directories separately
gulp.task('watch', function(){
    livereload.listen();
    for (var key in publishFolder){
        gulp.watch('./'+publishFolder[key], ['devcopy_'+key]);
    }
});

gulp.task('devcopy', devcopy_tasks);
gulp.task('clean', ['clean:devcopy', 'clean:build']);
gulp.task('build', build_tasks, function(){
    return gulp.src(buildFolder).
        pipe(zip(pkg.name+'-'+pkg.version+'.zip')).
        pipe(gulp.dest(buildBase));
});
gulp.task('default', ['clean', 'devcopy', 'watch']);

